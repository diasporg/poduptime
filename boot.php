<?php

/**
 * Boot the application, loading config and database connection.
 */

declare(strict_types=1);

use CodeZero\BrowserLocale\BrowserLocale;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Dotenv\Dotenv;
use DetectLanguage\DetectLanguage;
use RedBeanPHP\R;
use Laminas\Validator\File\Exists;

define('PODUPTIME', microtime(true));

require_once __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/.env');

if ($_SERVER['MAINTENANCE_MODE'] === 'true' && !isCli()) {
    echo 'Site offline for maintenance';
    die;
}

// Initialise language detection.
DetectLanguage::setApiKey($_SERVER['DETECTLANGUAGE_KEY']);
DetectLanguage::setSecure(true);

// Set up global DB connection.
R::setup(
    sprintf("pgsql:host=%s;port=%d;dbname=%s", $_SERVER['DB_HOST'], $_SERVER['DB_PORT'], $_SERVER['DB_DATABASE']),
    $_SERVER['DB_USERNAME'],
    $_SERVER['DB_PASSWORD'],
    true,
    true
);
R::testConnection() || die('Error in DB connection');

$browserlanguage = 'en';
if (!empty($_SERVER["HTTP_ACCEPT_LANGUAGE"])) {
    $browserlanguage = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
}

$browser = new BrowserLocale($browserlanguage);
$locales = $browser->getLocales();
$validator = new Exists();
foreach ($locales as $locale) {
    if ($validator->isValid($_SERVER['BASE_DIR'] . '/translations/base.' . $locale->language . '.yaml')) {
        $t = new Translator($locale->language);
        $t->addLoader('yaml', new YamlFileLoader());
        $t->addResource('yaml', $_SERVER['BASE_DIR'] . '/translations/base.' . $locale->language . '.yaml', $locale->language);
        break;
    } else {
        $t = new Translator('en');
        $t->addLoader('yaml', new YamlFileLoader());
        $t->addResource('yaml', $_SERVER['BASE_DIR'] . '/translations/base.en.yaml', 'en');
        break;
    }
}
