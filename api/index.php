<?php

global $queryType;

use GraphQL\GraphQL;
use GraphQL\Utils\BuildSchema;

require_once __DIR__ . '/../boot.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    try {
        $schema = BuildSchema::build(file_get_contents(__DIR__ . '/schema.graphql'));
        include __DIR__ . '/resolvers.php';

        $rawInput = file_get_contents('php://input');
        if ($rawInput === false) {
            throw new RuntimeException('Failed to get input');
        }

        $input = json_decode($rawInput, true);
        $query = $input['query'];
        $variableValues = $input['variables'] ?? null;

        podLog('API Query Received', $query);

        $result = GraphQL::executeQuery($schema, $query, $queryType, null, $variableValues);
    } catch (Throwable $e) {
        $result = [
            'error' => [
                'message' => $e->getMessage(),
            ],
        ];
    }

    header('Content-Type: application/json; charset=UTF-8');
    echo json_encode($result, JSON_THROW_ON_ERROR);
} else {
    require 'graphiql.php';
}
