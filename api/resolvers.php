<?php

use RedBeanPHP\R;
use Poduptime\PodStatus;
use Nette\Utils\Arrays;

require_once __DIR__ . '/../boot.php';

$queryType = [
    'nodes' => function ($_, array $args) {
        $sql = '
        SELECT 
         id,
         domain,
         name,
         metatitle,
         metadescription,
         metanodeinfo,
         metalocation,
         owner,
         metaimage,
         onion,
         i2p,
         terms,
         pp,
         support,
         zipcode,
         softwarename,
         masterversion,
         fullversion,
         shortversion,
         score,
         ip,
         detectedlanguage,
         country,
         countryname,
         city,
         state, 
         lat,
         long,
         ipv6,
         host,
         monthsmonitored,
         daysmonitored,
         signup,
         total_users, 
         active_users_halfyear,
         active_users_monthly,
         local_posts,
         uptime_alltime,
         status,
         latency,
         services,
         protocols,
         greenwebdata,
         greenhost,
         podmin_statement,
         podmin_notify,
         podmin_notify_level,
         dnssec,
         servertype,
         comment_counts,
         checks_count,
         date_updated,
         date_laststats,
         date_created,
         date_diedoff,
         date_lastlanguage
        FROM servers
        WHERE status = ? 
        AND softwarename NOT SIMILAR TO ? 
        AND domain NOT SIMILAR TO ? 
    ';
        $softwarename    = Arrays::get($args, 'softwarename', null);
        $ignoreblacklist = Arrays::get($args, 'ignoreblacklist', false);
        $minusersmonthly = Arrays::get($args, 'minusersmonthly', null);
        $status          = PodStatus::getAll()[Arrays::get($args, 'status', 'UP')];
        if ($ignoreblacklist) {
            $hiddensoftwares = "";
            $hiddendomains   = "";
        } else {
            $hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
            $hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);
        }
        if (!empty($softwarename) && isset($minusersmonthly)) {
            $sql .= ' AND softwarename = ? AND active_users_monthly > ? ORDER BY checks_count DESC';
            return R::getAll($sql, [$status, $hiddensoftwares, $hiddendomains, $softwarename, $minusersmonthly]);
        } elseif (!empty($softwarename)) {
            $sql .= ' AND softwarename = ? ORDER BY checks_count DESC';
            return R::getAll($sql, [$status, $hiddensoftwares, $hiddendomains, $softwarename]);
        } else {
            $sql .= ' ORDER BY checks_count DESC';
            return R::getAll($sql, [$status, $hiddensoftwares, $hiddendomains]);
        }
    },
    'node' => function ($_, array $args) {
        $domain = Arrays::get($args, 'domain');
        return R::getAll('
        SELECT 
         id,
         domain,
         name,
         metatitle,
         metadescription,
         metanodeinfo,
         metalocation,
         owner,
         metaimage,
         onion,
         i2p,
         terms,
         pp,
         support,
         zipcode,
         softwarename,
         masterversion,
         fullversion,
         shortversion,
         score,
         ip,
         detectedlanguage,
         country,
         countryname,
         city,
         state, 
         lat,
         long,
         ipv6,
         host,
         monthsmonitored,
         daysmonitored,
         signup,
         total_users, 
         active_users_halfyear,
         active_users_monthly,
         local_posts,
         uptime_alltime,
         status,
         latency,
         services,
         protocols,
         greenwebdata,
         greenhost,
         podmin_statement,
         podmin_notify,
         podmin_notify_level,
         dnssec,
         servertype,
         comment_counts,
         checks_count,
         date_updated,
         date_laststats,
         date_created,
         date_diedoff,
         date_lastlanguage
        FROM servers
        WHERE domain = ?
    ', [$domain]);
    },
    'checks' => function ($_, array $args) {
        $limit  = Arrays::get($args, 'limit', 5);
        $domain = Arrays::get($args, 'domain', '');
        $online = Arrays::get($args, 'online', '');
        $sql = '
        SELECT id, domain, online, error, latency, total_users, active_users_halfyear, active_users_monthly, local_posts, comment_counts, shortversion, version, date_checked
        FROM checks
        ';
        if ($domain && $online) {
            $sql .= ' WHERE domain = ? AND online = ? ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$domain, $online, $limit]);
        } elseif ($domain) {
            $sql .= ' WHERE domain = ? ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$domain, $limit]);
        } else {
            $sql .= ' ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$limit]);
        }
    },
    'clicks' => function ($_, array $args) {
        $limit  = Arrays::get($args, 'limit', 5);
        $domain = Arrays::get($args, 'domain', '');
        $sql = '
        SELECT id, domain, manualclick, autoclick, date_clicked
        FROM clicks
        ';
        if ($domain) {
            $sql .= ' WHERE domain = ? ORDER BY date_clicked DESC LIMIT ?';
            return R::getAll($sql, [$domain, $limit]);
        } else {
            $sql .= ' ORDER BY date_clicked DESC LIMIT ?';
            return R::getAll($sql, [$limit]);
        }
    },
    'monthlystats' => function ($_, array $args) {
        $sql = '
        SELECT id, softwarename, total_users, total_active_users_halfyear, total_active_users_monthly, total_posts, total_comments, total_pods, total_uptime, date_checked
        FROM monthlystats
        ';
        $softwarename = Arrays::get($args, 'softwarename', '');
        $mostcurrent  = Arrays::get($args, 'mostcurrent', '');
        if ($softwarename !== '' && $mostcurrent == 'true') {
            $sql .= ' WHERE softwarename = ? ORDER BY date_checked DESC LIMIT 1';
            return R::getAll($sql, [$softwarename]);
        } elseif ($softwarename !== '') {
            $sql  .= ' WHERE softwarename = ? ORDER BY date_checked DESC';
            return R::getAll($sql, [$softwarename]);
        } elseif ($mostcurrent == 'true') {
            $mostcurrentnumber = R::getAll('
             SELECT distinct COUNT(softwarename), date_checked
             FROM monthlystats
             WHERE softwarename != \'\'
             GROUP by date_checked order by date_checked desc limit 1
             ');
            $sql  .= ' ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$mostcurrentnumber[0]["count"]]);
        } else {
            $sql  .= ' ORDER BY date_checked DESC';
            return R::getAll($sql);
        }
    },
    'dailystats' => function ($_, array $args) {
        $sql = '
        SELECT id, softwarename, total_users, total_active_users_halfyear, total_active_users_monthly, total_posts, total_comments, total_pods, total_uptime, date_checked
        FROM monthlystats
        ';
        $softwarename = Arrays::get($args, 'softwarename', '');
        $mostcurrent  = Arrays::get($args, 'mostcurrent', '');
        if ($softwarename !== '' && $mostcurrent == 'true') {
            $sql .= ' WHERE softwarename = ? ORDER BY date_checked DESC LIMIT 1';
            return R::getAll($sql, [$softwarename]);
        } elseif ($softwarename !== '') {
            $sql  .= ' WHERE softwarename = ? ORDER BY date_checked DESC';
            return R::getAll($sql, [$softwarename]);
        } elseif ($mostcurrent == 'true') {
            $mostcurrentnumber = R::getAll('
             SELECT distinct COUNT(softwarename), date_checked
             FROM dailystats
             WHERE softwarename != \'\'
             GROUP by date_checked order by date_checked desc limit 1
             ');
            $sql  .= ' ORDER BY date_checked DESC LIMIT ?';
            return R::getAll($sql, [$mostcurrentnumber[0]["count"]]);
        } else {
            $sql  .= ' ORDER BY date_checked DESC';
            return R::getAll($sql);
        }
    },
    'masterversions' => function ($_, array $args) {
        return R::getAll('
        SELECT id, software, version, devlastcommit, releasedate, date_checked
        FROM masterversions
        ORDER BY id DESC
    ');
    },
    'history' => function ($_, array $args) {
        $limit  = Arrays::get($args, 'limit', 8000);
        $sql = '
        SELECT id, domain, change, note, date_changed
        FROM history
        ORDER BY id DESC
        LIMIT ?
    ';
        return R::getAll($sql, [$limit]);
    },
    'softwares' => function ($_, array $args) {
        return R::getAll('
        SELECT distinct softwarename, COUNT(id)
        FROM servers
        WHERE softwarename != \'\' AND status < 3
        GROUP by softwarename
    ');
    },
];

return [
    'Query'      => $queryType,
];
