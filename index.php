<?php

/**
 * Main entry point for site.
 */

declare(strict_types=1);

use RedBeanPHP\R;
use Poduptime\PodStatus;
use RedBeanPHP\RedException;

require_once __DIR__ . '/boot.php';
$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);
$maximumoftwares = (int) $_SERVER['MAX_COUNT_SOFTWARES'];

$input           = isset($_GET['input']) ? urldecode(substr($_GET['input'], 1)) : null;
podLog('Uservisit, page: ' . $input);
$mapview         = isset($_GET['map']) || $input === 'map';
$search          = isset($_GET['search']) || $input === 'search';
$go              = isset($_GET['go']) || $input === 'go';
$listview        = isset($_GET['list']) || $input === 'list';
$termsview       = isset($_GET['terms']) || $input === 'terms';
$statsview       = isset($_GET['stats']) || $input === 'stats';
$allsoftwares    = isset($_GET['allsoftwares']) || $input === 'allsoftwares';
$history         = isset($_GET['history']) || $input === 'history';
$dailystatsview  = isset($_GET['dailystats']) || $input === 'dailystats';
$podmin          = isset($_GET['podmin']) || $input === 'podmin';
$podminedit      = isset($_GET['podminedit']) || $input === 'podminedit';
$edit            = isset($_GET['edit']) || $input === 'edit';
$add             = isset($_POST['add']) || $input === 'add';
$gettoken        = isset($_POST['gettoken']) || $input === 'gettoken';
$admin           = isset($_GET['admin']) || $input === 'admin';
$status          = isset($_GET['status']) || $input === 'status';
$simpleview      = !($mapview || $podmin || $podminedit || $statsview);
$fullview        = false;
$subdomain       = join('.', explode('.', $_SERVER['HTTP_HOST'], -2));
$software_toggle = !empty($subdomain) ? $subdomain : 'All';
$software        = !empty($subdomain) ? ucwords($subdomain) : 'Fediverse';
$softwarejs      = !empty($subdomain) ? ucwords($subdomain) : '';
$softwaredb      = !empty($subdomain) ? $subdomain : 'all';
$software_all    = !empty($subdomain) ? ucwords($subdomain) : 'All';
$softwaren       = !empty($subdomain) ? '&software=' . $subdomain : '';


if ($go) {
    include_once __DIR__ . '/app/helpers/go.php';
}

try {
    $softwares = R::getAll('
        SELECT softwarename,
        sum(active_users_monthly) AS users
        FROM servers
        WHERE status < ? 
        AND score > 0
        AND softwarename NOT SIMILAR TO ? 
        AND softwarename !~* ?
        AND active_users_monthly IS NOT NULL 
        GROUP BY softwarename
        ORDER BY users desc, softwarename
        LIMIT ?
    ', [PodStatus::RECHECK, $hiddensoftwares, '\:|\'| |\.|/', $maximumoftwares]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

?>
<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="fediverse, lemmy, friendica, mastodon, peertube, pixelfed, misskey">
    <meta name="description" content="We observe all Fediverse Sites. Find a <?php echo $software ?> site to sign up for, find one close to you using a list, map or let us auto-pick one for you!">
    <link rel="icon" href="<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/main.min.css">
    <?php
    if ((!$statsview) && (!$dailystatsview) && (!$listview)) {
        print_r('<link rel="canonical" href="https://' . $_SERVER["DOMAIN"] . $_SERVER["REQUEST_URI"] . '">');
    }
    ?>
    <script src="<?php echo $_SERVER['CDN_DOMAIN'] ?>node_modules/jquery/dist/jquery.min.js"></script>
    <script defer src="<?php echo $_SERVER['CDN_DOMAIN'] ?>node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <meta property="og:url" content="https://<?php echo $_SERVER['HTTP_HOST'] ?>/<?php echo $input ?>">
    <meta property="og:title" content="<?php echo $_SERVER['LONG_TITLE'] ?>">
    <meta property="og:type" content="website">
    <meta property="og:description" content="<?php echo $software ?> Sites Status. Find a <?php echo $software ?> server to sign up for, find one close to you!">
    <title><?php echo $_SERVER['TITLE'] ?> | <?php echo $software ?> Sites Status</title>
</head>
<body class="d-flex flex-column h-100">
<input type="hidden" name="apiurl" value="<?php echo $_SERVER['API_LOCATION'] ?>">
    <nav class="shadow sticky-top navbar navbar-expand-lg navbar-dark">
        <div class="container-fluid">
        <a class="text-light navbar-brand fw-bold ms-2" href="/"><?php echo $_SERVER['TITLE'] ?></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <div class="navbar-toggler-icon"></div>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ms-auto my-auto">
                <li class="nav-item text-white my-auto fst-italic fw-bold me-lg-2">
                    <?php echo $t->trans('base.general.servers') ?>:
                </li>
                <li class="nav-item my-auto">
                    <a class="text-white nav-link" href="/list"><?php echo $t->trans('base.navs.list') ?></a>
                </li>
                <li class="nav-item my-auto">
                    <a class="text-white nav-link" href="/map"><?php echo $t->trans('base.navs.map') ?></a>
                </li>
                <li class="nav-item my-auto me-lg-5">
                    <?php
                    if (empty($subdomain)) {
                        echo '<a href="/go" class="text-white nav-link" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-custom-class="custom-tooltip" title="' . $t->trans('base.strings.list.navs.autotip', ['%(software)' => $softwarejs]) . '">' . $t->trans('base.strings.list.navs.auto') . '</a>';
                    } else {
                        echo '<a href="/go' . $softwaren . '" class="text-white nav-link" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-custom-class="custom-tooltip" title="' . $t->trans('base.strings.list.navs.autotip', ['%(software)' => $softwarejs]) . '">' . $t->trans('base.strings.list.navs.auto') . '</a>';
                    }
                    ?>
                </li>
                <li class="nav-item text-white my-auto fst-italic fw-bold me-lg-2">
                    <?php echo $t->trans('base.navs.stats') ?>:
                </li>
                <li class="nav-item my-auto">
                    <a href="/stats" class="text-white nav-link"><?php echo $t->trans('base.navs.monthlystats') ?></a>
                </li>
                <li class="nav-item my-auto me-lg-5">
                    <a href="/dailystats" class="text-white nav-link"><?php echo $t->trans('base.navs.dailystats') ?></a>
                </li>
                <div class="text-white navbar-text my-auto fst-italic fw-bold me-lg-2">
                <?php echo $t->trans('base.navs.software') ?>:
                </div>
                <li class="nav-item dropdown my-auto me-lg-5">
                    <a class="text-white nav-link dropdown-toggle" href="#" id="navbarDropdownMenuSoftwares" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                        <?php echo $software_toggle ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuSoftwares">
                        <a class="dropdown-item" href="//<?php echo $_SERVER['DOMAIN'] . strtok($_SERVER["REQUEST_URI"], '?') ?>">All</a>
                        <?php
                        foreach ($softwares as $software) {
                                printf(
                                    '<a class="dropdown-item" href="//%1$s.' . $_SERVER['DOMAIN'] . strtok(urldecode($_SERVER["REQUEST_URI"]), '?') . '">%1$s</a> ',
                                    $software['softwarename']
                                );
                        }
                        ?>
                        <a href="/allsoftwares">more</a>
                    </div>
                </li>
            </ul>
            <form class="d-flex ms-auto" action="/search" method="get">
                <input id="search" class="form-control me-2" name="query" type="search" placeholder="<?php echo $t->trans('base.navs.search') ?>" aria-label="Search">
            </form>
        </div>
        </div>
    </nav>
<main class="flex-shrink-0">
    <div class="main">
        <?php
        if ($mapview) {
            include_once __DIR__ . '/app/views/showmap.php';
        } elseif ($statsview) {
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/chart.js/dist/chart.umd.js"></script>');
            include_once __DIR__ . '/app/views/statsview.php';
        } elseif ($dailystatsview) {
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/chart.js/dist/chart.umd.js"></script>');
            include_once __DIR__ . '/app/views/dailystatsview.php';
        } elseif ($termsview) {
            include_once __DIR__ . '/app/views/termsview.php';
        } elseif ($search) {
            include_once __DIR__ . '/app/views/search.php';
        } elseif ($listview) {
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>');
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control.js"></script>');
            include_once __DIR__ . '/app/views/showfull.php';
        } elseif ($history) {
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/bootstrap-table/dist/bootstrap-table.min.js"></script>');
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/bootstrap-table/dist/extensions/filter-control/bootstrap-table-filter-control.js"></script>');
            include_once __DIR__ . '/app/views/history.php';
        } elseif ($podmin) {
            include_once __DIR__ . '/app/views/podmin.php';
        } elseif ($allsoftwares) {
            include_once __DIR__ . '/app/views/allsoftwares.php';
        } elseif ($admin && $_GET['adminkey'] == $_SERVER['ADMIN_KEY']) {
            include_once __DIR__ . '/app/views/admin.php';
        } elseif ($status) {
            include_once __DIR__ . '/app/views/status.php';
        } elseif ($edit) {
            include_once __DIR__ . '/app/functions/edit.php';
        } elseif ($add) {
            include_once __DIR__ . '/app/functions/add.php';
        } elseif ($gettoken) {
            include_once __DIR__ . '/app/functions/gettoken.php';
        } elseif ($input) {
            printf('<script src="' . $_SERVER['CDN_DOMAIN'] . 'node_modules/chart.js/dist/chart.umd.js"></script>');
            include_once __DIR__ . '/app/views/singleview.php';
        } else {
            include_once __DIR__ . '/app/views/welcome.php';
        }
        ?>
    </div>
</main>
<footer class="text-white footer mt-auto py-3">
    <div class="container">
        <div class="row-cols-1">
                    <?php
                    $csoftwares = c('softwares');
                    foreach ($csoftwares as $csoftware => $details) {
                        if ($csoftware === $subdomain && $details['info']) {
                            echo '<div class="row">
                                  <h5 class="text fw-bold d-flex justify-content-center">
                                  <a class="white d-flex justify-content-center" target="_new" href="' . $details['info'] . '">About ' . $subdomain . '</a></h5><p class="d-flex justify-content-center">';
                            echo $t->trans('softwares.' . $subdomain);
                            echo '</p></div>';
                        }
                    }
                        echo '
                    <div class="row d-flex justify-content-center">
                    <h5 class="text fw-bold d-flex justify-content-center">About ' . $_SERVER['TITLE'] . '</h5>
                    <p class="d-flex justify-content-center">' . $t->trans('base.strings.about', ['%(sitename)' => $_SERVER['TITLE']]) . '</p>
                    </div>
                    ';
                    ?>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        <div class="ps-2 col-md-auto"><a class="text-white-50" href="/history"><?php echo $t->trans('base.navs.history') ?></a></div>
        <div class="ps-2 col-md-auto"><a class="text-white-50" href="/podmin"><?php echo $t->trans('base.navs.admin') ?></a></div>
        <div class="ps-2 col-md-auto"><a class="text-white-50" href="/terms"><?php echo $t->trans('base.navs.terms') ?></a></div>
        <div class="ps-2 col-md-auto"><a class="text-white-50" href="https://gitlab.com/diasporg/poduptime"><?php echo $t->trans('base.navs.source') ?></a></div>
        <div class="ps-2 col-md-auto"><a class="text-white-50" href="<?php echo  $_SERVER['API_LOCATION'] ?>"><?php echo $t->trans('base.navs.api') ?></a></div>
        <div class="ps-2 col-md-auto"><a class="text-white-50" href="/status"><?php echo $t->trans('base.navs.status') ?></a></div>
        <?php
        if ($_SERVER['SERVER_AD_TEXT']) {
            echo "<div class='ps-2 col-md-auto'><a class='text-white-50' href='" . $_SERVER['SERVER_AD_URL'] . "'>" . $_SERVER['SERVER_AD_TEXT'] . "</a></div>";
        }
        ?>
    </div>
    <div class="toast-container position-fixed top-0">
        <div id="liveToast" class="toast text-bg-primary" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <div class="me-auto"><?php echo $t->trans('base.general.notice') ?></div>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body text-dark-emphasis" id="42">
            </div>
        </div>
    </div>
</footer>
<?php
$statsview && include_once __DIR__ . '/app/views/statsviewjs.php';
$dailystatsview && include_once __DIR__ . '/app/views/dailystatsviewjs.php';
?>
    <script>
        window.addEventListener('load', function () {
            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            })
        });
    </script>
<link rel="preload" href="<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/second.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
</body></html>
