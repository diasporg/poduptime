<?php

class FirstCest
{
    public function _before(AcceptanceTester $I)
    {
    }
    public function FrontPageLoads(AcceptanceTester $I)
    {
        // index.php loads welcome.php properly
        $I->amOnPage('/');
        $I->seeInSource('fediverse home');
    }

    public function MapPageLoads(AcceptanceTester $I)
    {
        // showmap.php loads map properly
        $I->amOnPage('/?map');
        $I->seeInSource('OpenStreetMap');
    }

    public function AddFail(AcceptanceTester $I)
    {
        // database/add.php fail on bad domain
        $I->sendAjaxPostRequest('/app/functions/add.php', ['domain' => 'xerox.com', 'email' => '', 'add' => 'save']);
        $I->expect('Could not validate');
    }

    public function AddFailDupe(AcceptanceTester $I)
    {
        // database/add.php fail on dupe domain
        $I->sendAjaxPostRequest('/app/functions/add.php', ['domain' => 'gesichtsbu.ch', 'email' => '', 'add' => 'save']);
        $I->expect('Server already exists');
    }

    public function CheckTableData(AcceptanceTester $I)
    {
        // validate server now on raw list data
        $I->amOnPage('/?list');
        $I->seeInSource('data-field');
    }

    public function CheckSearch(AcceptanceTester $I)
    {
        // validate server now on search results
        $I->amOnPage('/app/views/search.php?query=gesichtsbu.ch');
        $I->see('gesichtsbu.ch');
    }

    public function CheckStatus(AcceptanceTester $I)
    {
        // validate server now on search results
        $I->amOnPage('/app/views/status.php');
        $I->see('Green');
    }

    public function CheckStatusFrench(AcceptanceTester $I)
    {
        // validate server now on search results
        $I->setHeader("Accept-Language", "fr");
        $I->haveServerParameter('HTTP_ACCEPT_LANGUAGE', 'fr');
        $I->amOnPage('/app/views/status.php');
        $I->see('Vert');
    }

    public function ApiMainpage(AcceptanceTester $I)
    {
        // validate api index loads
        $I->amOnPage('/api/');
        $I->seeInSource('gitlab');
    }
}
