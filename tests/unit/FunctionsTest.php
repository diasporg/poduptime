<?php

use Codeception\Test\Unit;
use Poduptime\AddServerTask;
use Poduptime\UpdateServerTask;
use Poduptime\PodStatus;
use RedBeanPHP\RedException as RedExceptionAlias;

class FunctionsTest extends Unit
{
    public function testConfigHelper()
    {
        $this->assertIsArray(c());
        $this->assertStringContainsString('diaspora/diaspora', c('softwares')['diaspora']['repo']);
        $this->assertNull(c('invalid_key'));
        $this->assertSame('default', c('invalid_key', 'default'));
    }

    public function testCurl()
    {
        $curl = curl('icanhazip.com');
        $this->assertEquals(200, $curl['code']);
    }

    public function testUpdateMeta()
    {
        require_once __DIR__ . '/../../boot.php';
        $meta = addMeta('test', '42');
        $this->assertGreaterThanOrEqual(1, $meta);
    }

    public function testGetMeta()
    {
        require_once __DIR__ . '/../../boot.php';
        $meta = getMeta('test');
        $this->assertEquals('42', $meta);
    }

    public function testIpData()
    {
        $ipData = ipData('opensocial.at');
        $this->assertEquals('157.90.134.159', $ipData['ipv4'][0]);
        $this->assertEquals('2a01:4f8:110:5368::3', $ipData['ipv6'][0]);
        $this->assertNotEmpty($ipData['txt']);
    }

    public function testIpLocation()
    {
        $ipLocation = ipLocation('2.125.160.216');
        $this->assertNotEmpty($ipLocation['zipcode']);
    }

    public function testIpLocationException()
    {
        $ipLocation = ipLocation('983.234.324.324');
        $this->assertEmpty($ipLocation);
    }

    public function testIpDataIp4Only()
    {
        $ipData = ipData('ipv4.icanhazip.com');
        $this->assertArrayHasKey('ipv4', $ipData);
        $this->assertArrayNotHasKey('ipv6', $ipData);
    }

    public function testIpDataIp6Only()
    {
        $ipData = ipData('ipv6.icanhazip.com');
        $this->assertArrayHasKey('ipv6', $ipData);
        $this->assertArrayNotHasKey('ipv4', $ipData);
    }

    public function testSecondsToTime()
    {
        $this->assertSame('0 hours, 1 minutes', secondsToTime(-60));
        $this->assertSame('0 hours, 0 minutes', secondsToTime(0));
        $this->assertSame('0 hours, 0 minutes', secondsToTime(59));
        $this->assertSame('0 hours, 1 minutes', secondsToTime(60));
        $this->assertSame('1 hours, 0 minutes', secondsToTime(3_600));
        $this->assertSame('11 hours, 11 minutes', secondsToTime(40_260));
        $this->assertSame('999 hours, 59 minutes', secondsToTime(3_599_940));
        $this->assertSame('1h 2m 3s', secondsToTime(3723, '%3$dh %2$dm %1$ds'));
    }

    /**
     * @dataProvider curlPodsProviders
     */
    public function testExtractPodsFromCurl($curl, $field, $column, $pods)
    {
        $this->assertSame($pods, extractPodsFromCurl($curl, $field, $column));
    }

    public function curlPodsProviders(): \Generator
    {
        // https://diasp.org/pods.json
        yield [
            ['body' => '[{"host":"one.pod"},{"host":"two.pod"}]'],
            null,
            'host',
            ['one.pod', 'two.pod'],
        ];

        // https://the-federation.info/pods.json
        yield [
            ['body' => '{"pods":[{"host":"one.pod"},{"host":"two.pod"}]}'],
            'pods',
            'host',
            ['one.pod', 'two.pod'],
        ];

        // https://fedidb.org/api/v0/network/instances
        yield [
            ['body' => '["one.pod","two.pod"]'],
            null,
            null,
            ['one.pod', 'two.pod'],
        ];

        // https://instances.social/api/1.0/instances/list?count=0
        yield [
            ['body' => '{"instances":[{"name":"one.pod"},{"name":"two.pod"}]}'],
            'instances',
            'name',
            ['one.pod', 'two.pod'],
        ];
    }

    public function testNodeinfo()
    {
        $domain = nodeInfo('gesichtsbu.ch');
        $this->assertNotEmpty($domain['body']);
        $this->assertIsNumeric($domain['httpcode']);
    }

    public function testCheckNodeInfo()
    {
        $domain = checkNodeinfo('gesichtsbu.ch');
        $this->assertTrue($domain);
    }

    public function testCleanDomain()
    {
        $domain = cleanDomain('@gesichtsbu.ch/');
        $this->assertEquals('gesichtsbu.ch', $domain);
    }

    public function testMasterVersionCrawl()
    {
        $outcome = masterVersionCrawl();
        $this->assertTrue($outcome);
    }

    public function testISConnected()
    {
        $domain = isConnected();
        $this->assertTrue($domain);
    }

    public function testBackupData()
    {
        $backup = backupData();
        $this->assertFileExists($backup);
    }

    public function testOptimiseData()
    {
        $optimise = optimiseDatabase();
        $this->assertTrue($optimise);
    }

    public function testCleanData()
    {
        $clean = cleanDatabase();
        $this->assertFalse($clean);
    }

    public function testWriteCheck()
    {
        $write = writeCheck(["taco.gov", true, "555", "55.55555", "555", "54", "3", "555", "555", "5.5.5", "5.5.5-git42"]);
        $this->assertTrue($write);
    }

    public function testReadCheck()
    {
        $read = readCheck("taco.gov", true);
        $this->assertStringContainsStringIgnoringCase('5.5.5', $read['version']);
    }

    public function testPodLogCLI()
    {
        podLog('tacotuesday', 'taco.gov');
        $this->assertFileExists('log/script-' . $_SERVER['APP_ENV'] . '.log');
    }

    public function testPodLogHTTP()
    {
        global $ci;
        $ci = true;
        podLog('tacotuesday', 'taco.gov');
        $this->assertFileExists('log/user-' . $_SERVER['APP_ENV'] . '.log');
        $ci = false;
    }

    public function testIsCli()
    {
        $cli = isCli();
        $this->assertTrue($cli);
    }

    public function testTxtToQuery()
    {
        $newquery = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
        $this->assertEquals('%(nginx|wordpress)%', $newquery);
    }

    public function testAddServerWithPool()
    {
        require_once __DIR__ . '/../../boot.php';
        $added = addServer('gesichtsbu.ch', 'f');
        $this->assertTrue($added);
    }

    public function testAddServer()
    {
        require_once __DIR__ . '/../../boot.php';
        $task = new AddServerTask('diasp.eu', 'this is a test');
        $task();
        $this->assertStringContainsStringIgnoringCase('diasp.eu', file_get_contents('log/script-' . $_SERVER['APP_ENV'] . '.log'));
    }

    public function testUpdateServerWithPool()
    {
        $updated = updateServer('gesichtsbu.ch');
        $this->assertTrue($updated);
    }

    public function testUpdateServer()
    {
        $task = new UpdateServerTask('diasp.eu');
        $task();
        $this->assertStringContainsStringIgnoringCase('diasp.eu', file_get_contents('log/script-' . $_SERVER['APP_ENV'] . '.log'));
    }

    public function testAllServersList()
    {
        $servers = allServersList('diaspora');
        $this->assertArrayHasKey('0', $servers);
    }

    public function testAllDomainsData()
    {
        $servers = allDomainsData(null, true);
        $this->assertIsNumeric($servers[0]['count']);
    }

    public function testUpdateSitemap()
    {
        updateSitemap();
        $this->assertFileExists($_SERVER['SITEMAP']);
    }

    public function testClosestServers()
    {
        $servers = closestServers('2.125.160.216', 8);
        $this->assertIsArray($servers);
    }

    public function testUpdateRSS()
    {
        $atom = updateRSS();
        $this->assertStringContainsStringIgnoringCase('diasp.eu', $atom);
    }

    public function testGroupBy()
    {
        $array = groupBy(array(['1' => 'maybe', '2' => 'yes', '3' => 'no']), '3');
        $this->assertIsArray($array);
        $this->assertArrayHasKey('no', $array);
    }

    public function testReadServer()
    {
        $software = readServer('gesichtsbu.ch', 'softwarename');
        $this->assertEquals('mastodon', $software);
    }

    public function testWriteServer()
    {
         $domain = writeServer('gesichtsbu.ch', 'email', '1@2.3');
         $this->assertGreaterThan(0, $domain);
    }

    public function testReadServerAfter()
    {
        $software = readServer('gesichtsbu.ch', 'email');
        $this->assertEquals('1@2.3', $software);
    }

    public function testToast()
    {
        $text = toast('werewfdf');
        $this->assertStringContainsString('werewfdf', $text);
    }

    public function testUpdateALL()
    {
        require_once __DIR__ . '/../../boot.php';
        addMeta('pods_updating', true);
        $all = upateALL();
        addMeta('pods_updating', false);
        $this->assertEquals('All Servers Updated', $all);
    }

    public function testCrawlFediverse()
    {
        require_once __DIR__ . '/../../boot.php';
        addMeta('fediverse_updating', true);
        $crawl = crawlFediverse(true);
        addMeta('fediverse_updating', false);
        $this->assertEquals('Crawl Done', $crawl);
    }

    public function testUpdateDailyStatsToday()
    {
        require_once __DIR__ . '/../../boot.php';
        $daily = updateDailyStats('0');
        $this->assertEquals('Daily Done', $daily);
    }

    public function testUpdateDailyStatsYesterday()
    {
        require_once __DIR__ . '/../../boot.php';
        $daily = updateDailyStats('2');
        $this->assertEquals('Daily Done', $daily);
    }

    public function testUpdateMonthlyStats()
    {
        require_once __DIR__ . '/../../boot.php';
        $monthly = updateMonthlyStats();
        $this->assertEquals('Monthly Done', $monthly);
    }

    public function testServerStatus()
    {
        $status = PodStatus::USER_DELETED;
        $this->assertEquals('5', $status);
    }

    public function testOneServersData()
    {
        require_once __DIR__ . '/../../boot.php';
        $domain = oneDomainsData('gesichtsbu.ch');
        $this->assertEquals('mastodon', $domain['softwarename']);
    }

    public function testOneServersChecks()
    {
        require_once __DIR__ . '/../../boot.php';
        $checks = oneDomainsChecks('gesichtsbu.ch');
        $this->assertArrayHasKey('total_checks', $checks[0]);
    }

    public function testOneServersClicks()
    {
        require_once __DIR__ . '/../../boot.php';
        $clicks = oneDomainsClicks('gesichtsbu.ch');
        $this->assertArrayNotHasKey('manualclick', $clicks);
    }

    public function testPushUpdateAPI()
    {
        require_once __DIR__ . '/../../boot.php';
        $push = pushUpdateAPI();
        $this->assertEquals('Push Done', $push);
    }

    public function testSendEmail()
    {
        require_once __DIR__ . '/../../boot.php';
        $email = sendEmail('taco@mew.cat', 'Free Tacos', 'Taco Tuesday');
        $this->assertTrue($email);
    }

    public function testIsGreenHost()
    {
        $green = json_decode(isGreenHost('gesichtsbu.ch'));
        $this->assertTrue($green->green);
    }

    public function testChangeMMTrue()
    {
        changeMM('true');
        $this->assertStringContainsStringIgnoringCase('MAINTENANCE_MODE=true', file_get_contents('.env'));
    }

    public function testChangeMMFalse()
    {
        changeMM('false');
        $this->assertStringContainsStringIgnoringCase('MAINTENANCE_MODE=false', file_get_contents('.env'));
    }
}
