<?php

require_once __DIR__ . '/boot.php';

return
[
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/database/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/database/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_environment' => $_SERVER['APP_ENV'],
        'production' => [
            'adapter' => 'pgsql',
            'host' => $_SERVER['DB_HOST'],
            'name' => $_SERVER['DB_DATABASE'],
            'user' => $_SERVER['DB_USERNAME'],
            'pass' => $_SERVER['DB_PASSWORD'],
            'port' => $_SERVER['DB_PORT'],
            'charset' => 'utf8',
        ],
        'development' => [
            'adapter' => 'pgsql',
            'host' => $_SERVER['DB_HOST'],
            'name' => $_SERVER['DB_DATABASE'],
            'user' => $_SERVER['DB_USERNAME'],
            'pass' => $_SERVER['DB_PASSWORD'],
            'port' => $_SERVER['DB_PORT'],
            'charset' => 'utf8',
        ],
        'testing' => [
            'adapter' => 'pgsql',
            'host' => $_SERVER['DB_HOST'],
            'name' => $_SERVER['DB_DATABASE'],
            'user' => $_SERVER['DB_USERNAME'],
            'pass' => $_SERVER['DB_PASSWORD'],
            'port' => $_SERVER['DB_PORT'],
            'charset' => 'utf8',
        ]
    ],
    'version_order' => 'creation'
];
