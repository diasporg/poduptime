<?php

/**
 * Add a new pod.
 */

declare(strict_types=1);

use RedBeanPHP\R;
use RedBeanPHP\RedException;

require_once __DIR__ . '/../../boot.php';

$_domain              = cleanDomain($_POST['domain']) ?? null || die($t->trans('admin.noserver'));
$_email               = $_POST['email'] ?? '';

if (checkNodeinfo($_domain)) {
    $pod = R::findOne('servers', 'domain = ?', [$_domain]);
    if ($pod['email']) {
        echo $t->trans('admin.dupeserver');
        return;
    }
    if ($pod) {
        $key     = $pod['publickey'];
        $ip_data = ipData($pod['domain']);
        $txts    = str_replace('"', '', (array) $ip_data['txt']) ?? [];
        if (!in_array($key, $txts, true)) {
            echo $t->trans('admin.dnsrecord') . '<br><b>';
            echo $_domain . ' IN TXT "' . $pod['publickey'] . '"';
            echo '</b><br>';
            echo $t->trans('admin.dnsnote');
            return;
        } elseif (in_array($key, $txts, true) && $_email) {
            try {
                $pod['email'] = $_email;
                R::store($pod);
            } catch (RedException $e) {
                die('Error in SQL query: ' . $e->getMessage());
            }
            echo $t->trans('admin.emailsuccess') . '<br>';
        } elseif (!$_email) {
            echo $t->trans('admin.emailmissing') . '<br>';
        }
    }

    $success = addServer($_domain, 'manual server addition');
    if ($success) {
        $_email && sendEmail($_email, $t->trans('admin.addemailsubject'), $t->trans('admin.addemailbody', ['%(domain)' => $_domain]), $_SERVER['ADMIN_EMAIL']);
        echo $t->trans('admin.addsuccess');
    }
} else {
    echo $t->trans('admin.error', ['%(domain)' => $_domain]);
}
