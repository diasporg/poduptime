<?php

/**
 * Redirect to a given pod or find a good fit.
 */

declare(strict_types=1);

use Jaybizzle\CrawlerDetect\CrawlerDetect;
use RedBeanPHP\R;
use Poduptime\PodStatus;
use RedBeanPHP\RedException;
use RedBeanPHP\RedException\SQL;

// Other parameters.
$_domain   = $_GET['domain'] ?? '';
$_software = $_GET['software'] ?? '';

require_once __DIR__ . '/../../boot.php';

$country_code    = ipLocation($_SERVER['REMOTE_ADDR'])['country'];
$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

try {
    $sql = 'SELECT domain
            FROM servers
            WHERE signup
                AND uptime_alltime > 99
                AND monthsmonitored > 6
                AND status = ?
                AND softwarename NOT SIMILAR TO ?
                AND domain NOT SIMILAR TO ?
            ';
    $click  = 'autoclick';
    if ($_domain) {
        $click  = 'manualclick';
        $domain = R::getCell('SELECT domain FROM servers WHERE domain LIKE ?', [$_domain]);
    } elseif ($country_code && $_software) {
        $sql   .= ' AND softwarename = ? AND country = ? ORDER BY random() LIMIT 1';
        $domain = R::getCell($sql, [PodStatus::UP, $hiddensoftwares, $hiddendomains, $_software, $country_code]);
    } elseif ($_software) {
        $sql   .= ' AND softwarename = ? ORDER BY random() LIMIT 1';
        $domain = R::getCell($sql, [PodStatus::UP, $hiddensoftwares, $hiddendomains, $_software]);
    } elseif ($country_code) {
        $sql   .= ' AND country = ? ORDER BY random() LIMIT 1';
        $domain = R::getCell($sql, [PodStatus::UP, $hiddensoftwares, $hiddendomains, $country_code]);
    } else {
        $sql   .= ' ORDER BY random() LIMIT 1';
        $domain = R::getCell($sql, [PodStatus::UP, $hiddensoftwares, $hiddendomains]);
    }
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

if ($domain) {
    $c           = R::dispense('clicks');
    $c['domain'] = $domain;
    $c[$click]   = 1;
    if (!(new CrawlerDetect())->isCrawler()) {
        try {
            R::store($c);
        } catch (SQL $e) {
        }
    }
    podLog('go', $domain);
    header("link: <https://" . $domain . "/>; rel=\"canonical\"");
    header('Location: https://' . $domain);
    die();
} else {
    $goerror = true;
}
