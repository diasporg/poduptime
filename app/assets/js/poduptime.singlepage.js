$(document).ready(function () {
    var domain = $('input:input[name="domain"]').val();
    var apiurl = $('input:input[name="apiurl"]').val();
        let query = JSON.stringify({"query":"{node(domain: \"" + domain + "\"){name metatitle metadescription detectedlanguage ip ipv6 greenhost host servertype support softwarename shortversion masterversion daysmonitored date_laststats date_created city countryname uptime_alltime latency signup podmin_statement}}"})
        $.ajax({
            type: 'POST',
            url: apiurl,
            async: true,
            data: query,
            dataType: 'json',
            success: function (json) {
                $('#name').append(json.data.node[0].name);
                $('#metatitle').append(json.data.node[0].metatitle);
                $('#metadescription').append(json.data.node[0].metadescription);
                $('#detectedlanguage').append(json.data.node[0].detectedlanguage);
                $('#ip').append(json.data.node[0].ip);
                $('#ipv6').append(json.data.node[0].ipv6);
                $('#greenhost').append(json.data.node[0].greenhost ? 'Yes' : 'No');
                $('#host').append(json.data.node[0].host);
                $('#servertype').append(json.data.node[0].servertype);
                $('#support').append(json.data.node[0].support);
                $('#softwarename').append(json.data.node[0].softwarename);
                $('#shortversion').append(json.data.node[0].shortversion);
                $('#masterversion').append(json.data.node[0].masterversion);
                $('#daysmonitored').append(json.data.node[0].daysmonitored);
                $('#date_laststats').append(json.data.node[0].date_laststats);
                $('#date_created').append(json.data.node[0].date_created);
                $('#countryname').append(json.data.node[0].countryname);
                $('#city').append(json.data.node[0].city);
                $('#uptime_alltime').append(json.data.node[0].uptime_alltime + '%');
                $('#latency').append(json.data.node[0].latency + 'ms');
                $('#signup').append(json.data.node[0].signup ? 'Yes' : 'No');
                $('#podmin_statement').append(json.data.node[0].podmin_statement);
            },
            error: function (e) {
                alert(e.message);
            }
        });
});
