<?php

/**
 * Include JS for stats view.
 */

declare(strict_types=1);

use Poduptime\PodStatus;
use RedBeanPHP\R;
use RedBeanPHP\RedException;

require_once __DIR__ . '/../../boot.php';

$limit = ($_GET['months'] > 0 ? $_GET['months'] : $_SERVER['MONTHLY_STATS_DEFAULT']) ?? 6;
$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

try {
    $totalsusers = R::getAll('
        SELECT
            softwarename,
            count(*) AS pods,
            sum(total_users) AS users
        FROM servers
        WHERE status < ?
        AND score > 0
        AND softwarename IS NOT NULL
        AND softwarename != \'detecting\'
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
        GROUP BY softwarename
        ORDER BY users desc, softwarename limit 20
    ', [PodStatus::PAUSED, $hiddensoftwares, $hiddendomains]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $totalservers = R::getAll('
        SELECT
            softwarename,
            count(*) AS pods
        FROM servers
        WHERE status < ?
        AND score > 0         
        AND softwarename IS NOT NULL
        AND softwarename != \'detecting\' 
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
        GROUP BY softwarename
        ORDER BY pods desc, softwarename limit 20
    ', [PodStatus::PAUSED, $hiddensoftwares, $hiddendomains]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $totalsbycountry = R::getAll('
        SELECT
            count(*) AS pods,
            countryname
        FROM servers
        WHERE status < ?
        AND score > 0
        AND countryname IS NOT NULL
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
        GROUP BY countryname
        ORDER BY pods desc, countryname limit 20
    ', [PodStatus::PAUSED, $hiddensoftwares, $hiddendomains]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $totalsbyhost = R::getAll('
        SELECT
            count(*) AS pods,
            host
        FROM servers
        WHERE status < ?
        AND score > 0
        AND host IS NOT NULL
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
        GROUP BY host
        ORDER BY pods desc, host limit 20
    ', [PodStatus::PAUSED, $hiddensoftwares, $hiddendomains]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $totalsbygreen = R::getAll('
        SELECT
            count(*) AS pods,
            greenhost
        FROM servers
        WHERE status < ?
        AND score > 0
        AND greenhost IS NOT NULL
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
        GROUP BY greenhost
        ORDER BY pods desc, greenhost
    ', [PodStatus::PAUSED, $hiddensoftwares, $hiddendomains]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

$cf = R::getRow('
        SELECT count(id)
        FROM servers
        WHERE host = ?
    ', ['Cloudflare, Inc.']);
$black       = (array_column($totalsbygreen, 'pods')[1]);
$green       = (array_column($totalsbygreen, 'pods')[0]);
$ggreen      = ($green - $cf['count']);
$greens      = [$black,$ggreen,$cf['count']];
$greenlabels = ['No','Yes','CDN'];

try {
    $check_total_users = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_users AS users
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN now() - interval '$limit months' and now()
        GROUP BY yymm, users
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_users_active = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_active_users_halfyear AS active_users_halfyear,
            total_active_users_monthly AS active_users_monthly
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN now() - interval '$limit months' and now()
        GROUP BY yymm, active_users_halfyear, active_users_monthly
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_pods = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_pods AS pods
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN now() - interval '$limit months' and now()
        GROUP BY yymm, pods
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_comments = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_comments AS comments
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN now() - interval '$limit months' and now()
        GROUP BY yymm, comments
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $check_total_posts = R::getAll("
        SELECT
            to_char(date_checked, 'yyyy-mm') AS yymm,
            total_posts AS posts
        FROM monthlystats
        WHERE softwarename = ?
        AND date_checked BETWEEN now() - interval '$limit months' and now()
        GROUP BY yymm, posts
        ORDER BY yymm
    ", [$softwaredb]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}
?>
<script>
    $('select').on('change', function() {
        url = "/stats&months=" + this.value;
        window.location = url;
    });
    /**
     * Add a new pie chart for the passed data.
     *
     * @param id   HTML element ID to place the chart.
     * @param data Data to display on the chart.
     */
    function addPieChart(id, data) {
        var intToRGB = function(value, alpha, max) {
            var valueAsPercentageOfMax = value / max;
            // actual max is 16777215 but represnts white so we will take a max that is
            // below this to avoid white
            var MAX_RGB_INT = 16600000;
            var valueFromMaxRgbInt = Math.floor(MAX_RGB_INT * valueAsPercentageOfMax);
            //credit to https://stackoverflow.com/a/2262117/2737978 for the idea of how to implement
            var blue = Math.floor(valueFromMaxRgbInt % 256);
            var green = Math.floor(valueFromMaxRgbInt / 256 % 256);
            var red = Math.floor(valueFromMaxRgbInt / 256 / 256 % 256);
            return "rgba(" + red + "," + green + "," + blue + "," + alpha + ")";
        }
        var MAX = data.length;
        var backgroundColors = data.map(function(item) {
            return intToRGB(item, 0.8, MAX);
        });
        var borderColors = data.map(function(item) {
            return intToRGB(item, 1, MAX);
        });
        Chart.defaults.font.size = 18;
        new Chart(document.getElementById(id), {
            type: "pie",
            data: {
                labels: <?php echo json_encode(array_column($totalsusers, 'softwarename')); ?>,
                datasets: [{
                    data: data,
                    backgroundColor: backgroundColors,
                    borderColor: borderColors,
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                legend: {
                    display: false,
                    labels: {
                        fontSize: 16,
                        boxWidth: 4
                    }
                }
                }
            }
        });
    }
    function addPieChartCountry(id, data) {
        var intToRGB = function(value, alpha, max) {
            var valueAsPercentageOfMax = value / max;
            // actual max is 16777215 but represnts white so we will take a max that is
            // below this to avoid white
            var MAX_RGB_INT = 16600000;
            var valueFromMaxRgbInt = Math.floor(MAX_RGB_INT * valueAsPercentageOfMax);
            //credit to https://stackoverflow.com/a/2262117/2737978 for the idea of how to implement
            var blue = Math.floor(valueFromMaxRgbInt % 256);
            var green = Math.floor(valueFromMaxRgbInt / 256 % 256);
            var red = Math.floor(valueFromMaxRgbInt / 256 / 256 % 256);
            return "rgba(" + red + "," + green + "," + blue + "," + alpha + ")";
        }
        var MAX = data.length;
        var backgroundColors = data.map(function(item) {
            return intToRGB(item, 0.8, MAX);
        });
        var borderColors = data.map(function(item) {
            return intToRGB(item, 1, MAX);
        });
        new Chart(document.getElementById(id), {
            type: "pie",
            data: {
                labels: <?php echo json_encode(array_column($totalsbycountry, 'countryname')); ?>,
                datasets: [{
                    data: data,
                    backgroundColor: backgroundColors,
                    borderColor: borderColors,
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                        labels: {
                            fontSize: 16,
                            boxWidth: 4
                        }
                    }
                }
            }
        });
    }
    function addPieChartHost(id, data) {
        var intToRGB = function(value, alpha, max) {
            var valueAsPercentageOfMax = value / max;
            // actual max is 16777215 but represnts white so we will take a max that is
            // below this to avoid white
            var MAX_RGB_INT = 16600000;
            var valueFromMaxRgbInt = Math.floor(MAX_RGB_INT * valueAsPercentageOfMax);
            //credit to https://stackoverflow.com/a/2262117/2737978 for the idea of how to implement
            var blue = Math.floor(valueFromMaxRgbInt % 256);
            var green = Math.floor(valueFromMaxRgbInt / 256 % 256);
            var red = Math.floor(valueFromMaxRgbInt / 256 / 256 % 256);
            return "rgba(" + red + "," + green + "," + blue + "," + alpha + ")";
        }
        var MAX = data.length;
        var backgroundColors = data.map(function(item) {
            return intToRGB(item, 0.8, MAX);
        });
        var borderColors = data.map(function(item) {
            return intToRGB(item, 1, MAX);
        });
        new Chart(document.getElementById(id), {
            type: "pie",
            data: {
                labels: <?php echo json_encode(array_column($totalsbyhost, 'host')); ?>,
                datasets: [{
                    data: data,
                    backgroundColor: backgroundColors,
                    borderColor: borderColors,
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                        labels: {
                            fontSize: 16,
                            boxWidth: 4
                        }
                    }
                }
            }
        });
    }
    function addPieChartGreen(id, data) {
        new Chart(document.getElementById(id), {
            type: "pie",
            data: {
                labels: <?php echo json_encode($greenlabels); ?>,
                datasets: [{
                    data: data,
                    backgroundColor: ['#000','#008000','#f48120'],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                        labels: {
                            fontSize: 16,
                            boxWidth: 4
                        }
                    }
                }
            }
        });
    }
    /**
     * Add a new line chart for the passed data.
     *
     * @param id   HTML element ID to place the chart.
     * @param data Data to display on the chart.
     */
    function addLineChartusers(id, data) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_users, 'yymm')); ?>,
                datasets: [{
                    data: data,
                    label: '<?php echo $t->trans('base.general.users') ?>',
                    fill: false,
                    borderColor: "#304C89",
                    backgroundColor: "#304C89",
                    borderWidth: 3,
                    pointHoverRadius: 6,
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                scales: {
                    x: {
                        ticks: {
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            }
        });
    }
    function addLineChartusersactive(id, data1, data2) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_users_active, 'yymm')); ?>,
                datasets: [{
                    data: data1,
                    label: '<?php echo $t->trans('base.strings.stats.halfyear') ?>',
                    fill: false,
                    borderColor: "#648DE5",
                    backgroundColor: "#648DE5",
                    borderWidth: 3,
                    pointHoverRadius: 6
                },
                    {
                        data: data2,
                        label: '<?php echo $t->trans('base.strings.stats.monthly') ?>',
                        fill: false,
                        borderColor: "#548687",
                        backgroundColor: "#548687",
                        borderWidth: 3,
                        pointHoverRadius: 6
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                scales: {
                    x: {
                        ticks: {
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            }
        });
    }
    function addLineChartpods(id, data) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_pods, 'yymm')); ?>,
                datasets: [{
                    data: data,
                    label: '<?php echo $t->trans('base.general.servers') ?>',
                    fill: false,
                    borderColor: "#B0413E",
                    backgroundColor: "#B0413E",
                    borderWidth: 3,
                    pointHoverRadius: 6
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                scales: {
                    x: {
                        ticks: {
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            }
        });
    }
    function addLineChartcomments(id, data) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_comments, 'yymm')); ?>,
                datasets: [{
                    data: data,
                    label: '<?php echo $t->trans('base.general.comments') ?>',
                    fill: false,
                    borderColor: "#CDC392",
                    backgroundColor: "#CDC392",
                    borderWidth: 3,
                    pointHoverRadius: 6
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                scales: {
                    x: {
                        ticks: {
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            }
        });
    }
    function addLineChartposts(id, data) {
        new Chart(document.getElementById(id), {
            type: "line",
            data: {
                labels: <?php echo json_encode(array_column($check_total_posts, 'yymm')); ?>,
                datasets: [{
                    data: data,
                    label: '<?php echo $t->trans('base.general.posts') ?>',
                    fill: false,
                    borderColor: "#2B547E",
                    backgroundColor: "#2B547E",
                    borderWidth: 3,
                    pointHoverRadius: 6
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                    mode: 'index',
                    intersect: false,
                },
                scales: {
                    x: {
                        ticks: {
                            maxRotation: 90,
                            minRotation: 90
                        }
                    }
                }
            }
        });
    }
    addPieChart('total_network_users', <?php echo json_encode(array_column($totalsusers, 'users')); ?>);
    addPieChart('total_network_pods', <?php echo json_encode(array_column($totalservers, 'pods')); ?>);
    addPieChartCountry('total_servers_country', <?php echo json_encode(array_column($totalsbycountry, 'pods')); ?>);
    addPieChartHost('total_servers_host', <?php echo json_encode(array_column($totalsbyhost, 'pods')); ?>);
    addPieChartGreen('total_servers_green', <?php echo json_encode($greens); ?>);

    addLineChartusers('user_growth', <?php echo json_encode(array_column($check_total_users, 'users')); ?>);
    addLineChartusersactive('user_growth_active', <?php echo json_encode(array_column($check_total_users_active, 'active_users_halfyear')); ?>, <?php echo json_encode(array_column($check_total_users_active, 'active_users_monthly')); ?>);
    addLineChartpods('pod_growth', <?php echo json_encode(array_column($check_total_pods, 'pods')); ?>);
    addLineChartcomments('comment_growth', <?php echo json_encode(array_column($check_total_comments, 'comments')); ?>);
    addLineChartposts('posts_growth', <?php echo json_encode(array_column($check_total_posts, 'posts')); ?>);
</script>
