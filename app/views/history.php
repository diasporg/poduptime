<?php

/**
 * Show pod list table.
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

?>
<div class="col d-flex justify-content-center">
    <?php echo '<h2>' . $t->trans('base.strings.history.title') . '</h2>' ?>
</div>
<table id="table"
       data-toggle="table"
       data-pagination="true"
       data-ajax="ajaxRequest"
       data-show-toggle="true"
       data-show-fullscreen="true"
       data-show-columns="true"
       data-row-style="rowStyle"
       data-show-refresh="true"
       data-show-columns-toggle-all="true"
       data-detail-view="true"
       data-detail-view-by-click="true"
       data-detail-view-icon="false"
       data-filter-control="true"
       data-detail-formatter="detailFormatter"
       data-page-size="35"
       data-loading-template="loadingTemplate"
       data-page-list="[100, 250, 500, 1000, all]"
>
    <thead>
    <tr>
        <th data-sortable="true" data-filter-control="input" data-field="domain"><?php echo $t->trans('base.strings.history.domain') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="change"><?php echo $t->trans('base.strings.history.change') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="note"><?php echo $t->trans('base.strings.history.note') ?></th>
        <th data-sortable="true" data-filter-control="input" data-field="date_changed"><?php echo $t->trans('base.strings.history.datechanged') ?></th>
    </tr>
    </thead>
</table>
<script>
    var apiurl = $('input:input[name="apiurl"]').val();
    let host = window.location.host
    function ajaxRequest(params) {
        $.ajax({
            url: apiurl,
            method: 'POST',
            data: JSON.stringify({'query':'{history{domain change note date_changed}}'}),
            success: function (json) {
                params.success(json.data.history)
            }
        });
    }
    function detailFormatter(index, row) {
        let bob = ''
        bob += '<div class="text-brown"><a rel=”nofollow” target="_pod" class="text-brown text-decoration-none" href=//' + host + '/go&domain=' + row.domain + '>'
        + '<?php echo htmlspecialchars($t->trans('base.general.visitserver')) ?> <?php echo htmlspecialchars($t->trans('base.general.server')) ?> ' + row.domain + '</a></div>'
        + '<br><div class="text-brown"><a class="text-brown text-decoration-none" href=//' + host + '/' + row.domain + '><?php echo htmlspecialchars($t->trans('base.general.moreinfo')) ?></a></div>'
        return bob
    }
    function loadingTemplate(message) {
            return '<div class="spin spinner-border" role="status"><div class="sr-only"></div></div>'
    }
    function rowStyle(row, index) {
        if (index % 2 === 0) {
            return {
                css: {
                    background: '#fafafa'
                }
            }
        }
        return {
            css: {
                background: '#dddddd'
            }
        }
    }

</script>
<?php echo $t->trans('base.strings.history.tip') ?>

