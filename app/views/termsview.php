    <!-- These terms of service and privacy policy documents have been forked -->
    <!-- from the App.Net ToS repository (https://github.com/appdotnet/terms-of-service). -->
    <!-- The terms of use and privacy policy are available to you under the -->
    <!-- CC BY-SA 3.0 Creative Commons license. -->
    <!-- If you modify or copy this file, please do not remove this license text. -->
    <div class='container' id='terms'>
            <div class='row'>
                <div class='col-md-8 col-md-offset-2 tab-content'>
                    <div>
                        <div class='page-header'>
                            <h1>
                                <?php echo $_SERVER['TITLE'] ?>- Terms of Service
                                <small>
                                    Last Updated: 31st December, 2024
                                </small>
                            </h1>
                        </div>
                        <p>
                            Here are the important things you need to know about accessing and using the <strong><?php echo $_SERVER['TITLE'] ?></strong> (<?php echo $_SERVER['DOMAIN'] ?>) website and service (collectively, "Service"). These are our terms of service ("Terms"). Please read them carefully.
                        </p>
                        <h2>
                            Accepting these Terms
                        </h2>
                        <p>
                            If you access or use the Service, it means you agree to be bound by all of the terms below. So, before you use the Service, please read all of the terms. If you don't agree to all of the terms below, please do not use the Service.
                            Also, if a term does not make sense to you, please let us know at <a href="https://gitlab.com/diasporg/poduptime/-/issues/new">https://gitlab.com/diasporg/poduptime/-/issues/new</a>.
                        </p>
                        <h2>
                            Changes to these Terms
                        </h2>
                        <p>
                            We reserve the right to modify these Terms at any time. For instance, we may need to change these Terms if we come out with a new feature or for some other reason.
                        </p>
                        <p>
                            Whenever we make changes to these Terms, the changes are effective six (6) weeks after we post such revised Terms to our website (indicated by revising the date at the top of these Terms), or upon your acceptance if we provide a mechanism for your immediate acceptance of the revised Terms (such as a click-through confirmation or acceptance button). It is your responsibility to check the website regularly for changes to these Terms.
                        </p>
                        <p>
                            If you continue to use the Service after the revised Terms go into effect, then you have accepted the revised Terms.
                        </p>
                        <h2>
                            Privacy Policy
                        </h2>
                        <p>
                            For information about how we collect and use information about users of the Service, please check out our <a href="#privacy" aria-controls="privacy" data-toggle="tab">privacy policy</a>.
                        </p>
                        <h2>
                            Third-Party Services
                        </h2>
                        <p>
                            From time to time, we may provide you with links to third party websites or services that we do not own or control. Your use of the Service may also include the use of applications that are developed or owned by a third party. Your use of such third party applications, websites, and services is governed by that party’s own terms of service or privacy policies. We encourage you to read the terms and conditions and privacy policy of any third party application, website or service that you visit or use. Note that while <strong><?php echo $_SERVER['TITLE'] ?></strong> itself does not work directly with advertisers, third party applications may contain advertising or marketing materials provided by such third parties.
                        </p>

                        <h2>
                            Your Content & Conduct
                        </h2>
                        <p>
                            We cannot be held responsible should a programming or administrative error make your content visible to a larger audience than you had intended.
                        </p>

                        <p>
                            You agree that you will not do any of the following in connection with the Service or other users:
                        </p>
                        <ul>
                            <li>
                                Use the Service in any manner that could interfere with, disrupt, negatively affect or inhibit other users from fully enjoying the Service or that could damage, disable, overburden or impair the functioning of the Service;
                            </li>
                            <li>
                                Create an account or post any content if you are not over 18 years of age;
                            </li>
                            <li>
                                Circumvent or attempt to circumvent any filtering, security measures, rate limits or other features designed to protect the Service, users of the Service, or third parties.
                            </li>
                        </ul>
                        <h2>
                            Source code and materials
                        </h2>
                        <p>
                            This Service runs on the Poduptime codebase. This source code is licensed under an
                            <a href="https://www.gnu.org/licenses/agpl-3.0.html">AGPLv3</a> license which means you are allowed
                            to and even encouraged to take the source code, modify it and use it.
                        </p>
                        <p>
                            For full details about Poduptime <a href="https://gitlab.com/diasporg/poduptime">see here</a>. Very generally we look for open data from nodeinfo on servers and open APIs to gather stats, data and uptime for servers we find. Some servers share more data like user numbers some share very little, that is by design, we only gather what we can find.
                        </p>
                        <h2>
                            Hyperlinks and Third Party Content
                        </h2>
                        <p>
                            <strong><?php echo $_SERVER['TITLE'] ?></strong> makes no claim or representation regarding, and accepts no responsibility for third party websites accessible by hyperlink from the Service or websites linking to the Service. When you leave the Service, you should be aware that these Terms and our policies no longer govern.
                        </p>
                        <p>
                            A lot of the content on the Service is from you and others, and we don't review, verify or authenticate it, and it may include inaccuracies or false information. We make no representations, warranties, or guarantees relating to the quality, suitability, truth, accuracy or completeness of any content contained in the Service. You acknowledge sole responsibility for and assume all risk arising from your use of or reliance on any content.
                        </p>
                        <h2>
                            Unavoidable Legal Stuff
                        </h2>
                        <p>
                            <strong>The Service and any other service and content included on or otherwise made available to you through the Service are provided to you on an as is or as available basis without any representations or warranties of any kind. We disclaim any and all warranties and representations (express or implied, oral or written) with respect to the Service and content included on or otherwise made available to you through the Service whether alleged to arise by operation of law, by reason of custom or usage in the trade, by course of dealing or otherwise.</strong>
                        </p>
                        <p>
                            <strong>In no event will <strong><?php echo $_SERVER['TITLE'] ?></strong> be liable to you or any third party for any special, indirect, incidental, exemplary or consequential damages of any kind arising out of or in connection with the Service or any other service and/or content included on or otherwise made available to you through the Service, regardless of the form of action, whether in contract, tort, strict liability or otherwise, even if we have been advised of the possibility of such damages or are aware of the possibility of such damages. This section will be given full effect even if any remedy specified in this agreement is deemed to have failed of its essential purpose.</strong>
                        </p>
                        <p>
                            You agree to defend, indemnify and hold us harmless from and against any and all costs, damages, liabilities, and expenses (including attorneys' fees, costs, penalties, interest and disbursements) we incur in relation to, arising from, or for the purpose of avoiding, any claim or demand from a third party relating to your use of the Service or the use of the Service by any person using your account, including any claim that your use of the Service violates any applicable law or regulation, or the rights of any third party, and/or your violation of these Terms.
                        </p>
                        <h2>
                            Governing Law
                        </h2>
                        <p>
                            The validity of these Terms and the rights, obligations, and relations of the parties under these Terms will be construed and determined under and in accordance with the laws of Oregon, USA.
                        </p>
                        <h2>
                            Jurisdiction & Waiver of Representative Actions
                        </h2>
                        <p>
                            You expressly agree that exclusive jurisdiction for any dispute with <strong><?php echo $_SERVER['TITLE'] ?></strong>, or in any way relating to your use of the <strong><?php echo $_SERVER['TITLE'] ?></strong> website or Service, resides in the courts of Oregon, USA and you further agree and expressly consent to the exercise of personal jurisdiction in the courts of Oregon, USA in connection with any such dispute including any claim involving <strong><?php echo $_SERVER['TITLE'] ?></strong>. You further agree that you and <strong><?php echo $_SERVER['TITLE'] ?></strong> will not commence against the other a class action, class arbitration or other representative action or proceeding.
                        </p>
                        <h2>
                            Termination
                        </h2>
                        <p>
                            If you breach any of these Terms, we have the right to suspend or disable your access to or use of the Service.
                        </p>
                        <h2>
                            Entire Agreement
                        </h2>
                        <p>
                            These Terms constitute the entire agreement between you and <strong><?php echo $_SERVER['TITLE'] ?></strong> regarding the use of the Service, superseding any prior agreements between you and <strong><?php echo $_SERVER['TITLE'] ?></strong> relating to your use of the Service.
                        </p>
                        <h2>
                            Questions & Contact Information
                        </h2>
                        <p>
                            Questions or comments about the Service may be directed to us at <a href="https://gitlab.com/diasporg/poduptime/-/issues/new">https://gitlab.com/diasporg/poduptime/-/issues/new</a>.
                        </p>
                    </div>
                    <div>
                        <div class='page-header'>
                            <h1>
                                <?php echo $_SERVER['TITLE'] ?> - Privacy Policy
                            </h1>
                        </div>
                        <p>
                            Our privacy policy applies to information we collect when you use or access our website located at <?php echo $_SERVER['DOMAIN'] ?> and social networking services, or just interact with us. We may change this privacy policy from time to time. Whenever we make changes to this privacy policy, the changes are effective six (6) weeks after we post the revised privacy policy to our website (as indicated by revising the date at the top of our privacy policy). We encourage you to review our privacy policy whenever you access our services to stay informed about our information practices and the ways you can help protect your privacy.
                        </p>
                        <h2>
                            Collection of Information
                        </h2>
                        <h3>
                            Information You Provide to Us
                        </h3>
                        <p>
                            We collect information you provide directly to us. For example, we collect information when you create an account, subscribe, participate in any interactive features of our services, fill out a form, request customer support or otherwise communicate with us. The types of information we may collect include your name (real of fake), email address and other contact or identifying information you choose to provide.
                        </p>
                        <h3>
                            Information We Collect Automatically When You Use the Services
                        </h3>
                        <p>
                            When you access or use our services, we automatically collect information about you, including:
                        </p>
                        <dl>
                            <dt>
                                Log Information
                            </dt>
                            <dd>
                                We may log information about your use of our services, including the type of browser you use, access times, pages viewed, your IP address and the page you visited before navigating to our services.
                            </dd>
                            <dt>
                                Device Information
                            </dt>
                            <dd>
                                We may collect information about the computer you use to access our services, including the hardware model, and operating system and version.
                            </dd>
                        </dl>
                        <h2>
                            Use of Information
                        </h2>
                        <p>
                            We do not use your information for serving up ads.
                        </p>
                        <p>
                            We may use information about you for various purposes, including to:
                        </p>
                        <ul>
                            <li>
                                Provide, maintain and improve our services;
                            </li>
                            <li>
                                Send you technical notices, updates, security alerts and support and administrative messages;
                            </li>
                            <li>
                                Respond to your comments, questions and requests;
                            </li>
                            <li>
                                Communicate with you about <strong><?php echo $_SERVER['TITLE'] ?></strong>-related news and information;
                            </li>
                            <li>
                                Monitor and analyze trends, usage and activities in connection with our services; and
                            </li>
                            <li>
                                Personalize and improve our services.
                            </li>
                        </ul>
                        <h2>
                            Sharing of Information
                        </h2>
                        <p>
                            Remember, we don't share your information with advertisers.
                        </p>
                        <p>
                            We may share personal information about you as follows:
                        </p>
                        <ul>
                            <li>
                                If we believe disclosure is reasonably necessary to comply with any applicable law, regulation, legal process or governmental request;
                            </li>
                            <li>
                                To enforce applicable user agreements or policies, including our <a href="/terms#terms">terms of service</a>; and to protect <strong><?php echo $_SERVER['TITLE'] ?></strong>, our users or the public from harm or illegal activities;
                            </li>
                            <li>
                                In connection with any merger, sale of <strong><?php echo $_SERVER['TITLE'] ?></strong> assets, financing or acquisition of all or a portion of our business to another company or individual; and
                            </li>
                            <li>
                                If we notify you through our services (or in our privacy policy) that the information you provide will be shared in a particular manner and you provide such information.
                            </li>
                        </ul>
                        <p>
                            We may also share aggregated or anonymized information that does not directly identify you.
                        </p>
                        <h2>
                            Security
                        </h2>
                        <p>
                            <strong><?php echo $_SERVER['TITLE'] ?></strong> takes reasonable measures to help protect personal information from loss, theft, misuse and unauthorized access, disclosure, alteration and destruction.
                        </p>
                        <h2>
                            Your Information Choices
                        </h2>
                        <h3>
                            Cookies
                        </h3>
                        <p>
                            Most web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove or reject browser cookies. Please note that if you choose to remove or reject cookies, this could affect the availability and functionality of our services.
                        </p>
                        <h2>
                            Contact Us
                        </h2>
                        <p>
                            If you have any questions about this privacy policy, please contact us at <a href="https://gitlab.com/diasporg/poduptime/-/issues/new">https://gitlab.com/diasporg/poduptime/-/issues/new</a>.
                        </p>
                    </div>
                </div>
            </div>
        </div>
