<?php

/**
 * This is just a single pod data pull for /domain.tld page
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

use Matriphe\ISO639\ISO639;
use Carbon\Carbon;

global $goerror;
$_domain = 'diasp.org';
$input           = isset($_GET['input']) ? substr($_GET['input'], 1) : null;
$deletedview     = isset($_GET['deletedview']);
$iso             = new ISO639();
$_domain         = strtolower(idn_to_utf8($input));
$pod             = oneDomainsData($_domain);
$totals          = oneDomainsChecks($_domain);

printf('<script defer src="' . $_SERVER['CDN_DOMAIN'] . 'app/assets/js/poduptime.singlepage.min.js"></script>');

if ((isset($pod['domain']) && $pod['status'] < 3) || ($deletedview == 1)) {
    $humanmonitored = Carbon::createFromTimeStamp(strtotime($pod['date_created']))->locale($locale->language)->diffForHumans();
    ?>
    <div class="w-auto p-lg-4">
    <input type="hidden" name="domain" value="<?php echo $pod['domain'] ?>">
    <a rel=”nofollow” class="text-brown lead p-0 m-0 mb-1" target="go" href="/go&domain=<?php echo $pod['domain'] ?>">
        <h1 class="fw-bold mb-lg-4"><?php echo $pod['domain'] ?></h1>
    </a>
    <div class="shadow-lg w-auto mb-lg-4">
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.name') ?></div>
            <div class="col fw-bold" id="name">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.metatitle') ?></div>
            <div class="col fw-bold" id="metatitle">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2 bg-ed"><?php echo $t->trans('base.strings.singlepage.metadescription') ?></div>
            <div class="col bg-ed fw-bold" id="metadescription">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.detectedlanguage') ?></div>
            <div class="col fw-bold" id="detectedlanguage">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.ip') ?></div>
            <div class="col fw-bold" id="ip">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.ipv6') ?></div>
            <div class="col fw-bold" id="ipv6">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.greenhost') ?></div>
            <div class="col fw-bold" id="greenhost">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.host') ?></div>
            <div class="col fw-bold" id="host">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.servertype') ?></div>
            <div class="col fw-bold" id="servertype">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.support') ?></div>
            <div class="col fw-bold" id="support">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.softwarename') ?></div>
            <div class="col fw-bold" id="softwarename">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.shortversion') ?></div>
            <div class="col fw-bold" id="shortversion">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.masterversion') ?></div>
            <div class="col fw-bold" id="masterversion">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.daysmonitored') ?></div>
            <div class="col fw-bold" id="daysmonitored">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.date_laststats') ?></div>
            <div class="col fw-bold" id="date_laststats">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.date_created') ?></div>
            <div class="col fw-bold" id="date_created">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.countryname') ?></div>
            <div class="col fw-bold" id="countryname">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.city') ?></div>
            <div class="col fw-bold" id="city">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.uptime_alltime') ?></div>
            <div class="col fw-bold" id="uptime_alltime">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.latency') ?></div>
            <div class="col fw-bold" id="latency">&nbsp;</div>
        </div>
        <div class="row bg-gray">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.signup') ?></div>
            <div class="col fw-bold" id="signup">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-md-2"><?php echo $t->trans('base.strings.singlepage.podmin_statement') ?></div>
            <div class="col fw-bold" id="podmin_statement">&nbsp;</div>
        </div>
        </div>
    </div>
    <?php

    echo  '<div class="align-items-center row"><h5 class="fw-bold">' . $t->trans('base.strings.singlepage.uptime') . '</h5></div><div class="d-flex w-auto p-md-5 align-items-center row shadow-lg mb-lg-4">';
    include 'podstat-uptime.php';
    echo '</div><div class="align-items-center row"><h5 class="fw-bold">' . $t->trans('base.strings.singlepage.userstats') . '</h5></div><div class="d-flex w-auto p-md-5 align-items-center row shadow-lg mb-lg-4">';
    include 'podstat-user-counts.php';
    echo '</div><div class="align-items-center row"><h5 class="fw-bold">' . $t->trans('base.strings.singlepage.actionstats') . '</h5></div><div class="d-flex w-auto p-md-5 align-items-center row shadow-lg mb-lg-4">';
    include 'podstat-actions-counts.php';
    ?>
        </div>
    <div class="fw-light fs-6"><?php echo $t->trans('base.general.api') ?></div>

    <?php
} elseif (isset($pod['domain']) && $pod['status'] > 3) {
    ?>
    <div class="container">
        <div class="text-justify row row-cols-1 p-2">
            <h3 class="alert alert-danger" role="alert"><?php echo $t->trans('base.strings.singlepage.deleted') ?></h3>
            <br><a rel=”nofollow” href="https://<?php echo $pod['softwarename'] . '.' . $_SERVER['DOMAIN'] ?>"><h4><?php echo $t->trans('base.strings.singlepage.findother', ['%(software)' => $pod['softwarename']]) ?></h4></a><br><br>
            <br class="pt-5 mt-5"><a rel=”nofollow” href="/<?php echo $_domain ?>&deletedview=yes"><?php echo $t->trans('base.strings.singlepage.deletedview') ?></a>
            <div class="fw-light fs-6"><?php echo $t->trans('base.general.api') ?></div>
        </div>
    </div>
    <?php
} elseif ($goerror) {
    ?>
    <div class="container">
        <div class="text-justify row row-cols-1 p-2">
        <h1 class="alert alert-danger" role="alert"><?php echo $t->trans('base.strings.singlepage.goerror') ?></h1>
        </div>
    </div>
    <?php
} else {
    podLog('Singlepage Missing', $_domain, 'warning');
    ?>
    <div class="container">
        <div class="text-justify row row-cols-1 p-2">
            <h1 class="alert alert-danger" role="alert"><?php echo $t->trans('base.strings.singlepage.notfound') ?></h1>
        </div>
    </div>
    <?php
}
