<?php

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';
$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

use RedBeanPHP\R;
use Poduptime\PodStatus;
use RedBeanPHP\RedException;

try {
    $softwares = R::getAll('
        SELECT softwarename,
        sum(active_users_monthly) AS users
        FROM servers
        WHERE status < ? 
        AND score > 0
        AND softwarename NOT SIMILAR TO ? 
        AND softwarename !~* ?
        AND active_users_monthly IS NOT NULL 
        GROUP BY softwarename
        ORDER BY users desc, softwarename
    ', [PodStatus::RECHECK, $hiddensoftwares, '\:|\'| |\.|/']);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

echo "<h1>" . $t->trans('base.strings.allsoftwares.title') . "</h1>";
?>
<div class='float-end pe-1'>
    <img class='img-fluid img-thumbnail' src='<?php echo $_SERVER['CDN_DOMAIN'] ?>app/assets/images/fediverse-branches-axbom-30-CC-BY-SA.png' alt='The many branches of the Fediverse'>
<a class="d-block" href="https://axbom.com/fediverse/">https://axbom.com/fediverse/</a>
</div>
    <table class="w-25 table table-striped table-responsive">
        <thead>
        <tr>
            <th scope="col"><?php echo $t->trans('base.strings.list.columns.software') ?></th>
            <th scope="col"><?php echo $t->trans('base.strings.list.columns.activeusers') ?></th>
            <th scope="col"><?php echo $t->trans('base.navs.list') ?></th>
            <th scope="col"><?php echo $t->trans('base.navs.map') ?></th>
            <th scope="col"><?php echo $t->trans('base.navs.monthlystats') ?></th>
        </tr>
        </thead>
        <tbody>

<?php
foreach ($softwares as $software) {
    echo "<tr>";
    printf(
        '<td>%1$s</td><td>%2$s</td><td><a href="//%1$s.' . $_SERVER['DOMAIN'] . '/list"><img src="' . $_SERVER['CDN_DOMAIN'] . 'app/assets/images/list-columns.svg" alt="List View" width="32" height="32"></a></td><td><a href="//%1$s.' . $_SERVER['DOMAIN'] . '/map"><img src="' . $_SERVER['CDN_DOMAIN'] . 'app/assets/images/map.svg" alt="Map View" width="32" height="32"></a></td><td><a href="//%1$s.' . $_SERVER['DOMAIN'] . '/stats"><img src="' . $_SERVER['CDN_DOMAIN'] . 'app/assets/images/graph.svg" alt="Stats View" width="32" height="32"></a></td>',
        $software['softwarename'],
        $software['users']
    );
    echo "</tr>";
    $cc++;
}

echo "</tbody></table>";
echo $cc . " " . $t->trans('base.strings.stats.total');

