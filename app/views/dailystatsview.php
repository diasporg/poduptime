<div class="container-fluid">
    <div class="shadow-lg p-3 mb-5 bg-body rounded">
    <h1 class="text-center"><?php echo $t->trans('base.strings.dailystats.title') ?></h1>
    <div class="row justify-content-center">
        <div class="col-lg-2 justify-content-center text-center">
            <h4><?php echo $t->trans('base.strings.dailystats.users') ?></h4>
            <div class="justify-content-center">
                <canvas id="total_network_users"></canvas>
            </div>
        </div>
        <div class="col-lg-2  justify-content-center text-center">
            <h4><?php echo $t->trans('base.strings.dailystats.serverss') ?></h4>
            <div class="justify-content-center">
                <canvas id="total_network_pods"></canvas>
            </div>
        </div>
        <div class="col-lg-2 justify-content-center text-center">
            <h4><?php echo $t->trans('base.strings.dailystats.serversc') ?></h4>
            <div class="justify-content-center">
                <canvas id="total_servers_country"></canvas>
            </div>
        </div>
        <div class="col-lg-2 justify-content-center text-center">
            <h4><?php echo $t->trans('base.strings.dailystats.serversh') ?></h4>
            <div class="justify-content-center">
                <canvas id="total_servers_host"></canvas>
            </div>
        </div>
        <div class="col-lg-2 justify-content-center text-center">
            <h4><?php echo $t->trans('base.strings.dailystats.serversgreen') ?></h4>
            <div class="justify-content-center">
                <canvas id="total_servers_green"></canvas>
            </div>
        </div>
    </div>
    </div>
    <select class="w-auto form-select form-select-sm" aria-label=".form-select-sm">
        <option selected><?php echo $t->trans('base.navs.days') ?></option>
        <option value="7">7</option>
        <option value="30">30</option>
        <option value="60">60</option>
        <option value="90">90</option>
        <option value="120">120</option>
    </select>
    <br>
    <h1 class="text-center p-1"><?php echo $t->trans('base.strings.dailystats.statsfor') . ' ' ?> <?php echo $software_all ?></h1>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <h4><?php echo $t->trans('base.strings.dailystats.total') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.dailystats.growthactive') ?></h4>
        </div>
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <canvas class="d-flex w-100 p-md-5 justify-content-center" id="user_growth_active"></canvas>
        </div>
    </div>
        <div class="row justify-content-center p-1">
            <div class="d-flex w-100 p-md-5 justify-content-center">
                <h4><?php echo $t->trans('base.strings.dailystats.total') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.dailystats.growth') ?></h4>
            </div>
            <div class="d-flex w-100 p-md-5 justify-content-center">
                <canvas class="d-flex w-100 p-md-5 justify-content-center" id="user_growth"></canvas>
            </div>
        </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <h4><?php echo $t->trans('base.strings.dailystats.total') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.dailystats.serversper') ?></h4>
        </div>
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <canvas class="d-flex w-100 p-md-5 justify-content-center" id="pod_growth"></canvas>
        </div>
    </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <h4><?php echo $t->trans('base.strings.dailystats.total') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.dailystats.commentsper') ?></h4>
        </div>
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <canvas class="d-flex w-100 p-md-5 justify-content-center" id="comment_growth"></canvas>
        </div>
    </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <h4><?php echo $t->trans('base.strings.dailystats.total') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.dailystats.postsper') ?></h4>
        </div>
        <div class="d-flex w-100 p-md-5 justify-content-center">
            <canvas class="d-flex w-100 p-md-5 justify-content-center" id="posts_growth"></canvas>
        </div>
    </div>
    <?php echo '<div>' . $t->trans('base.strings.dailystats.tip')  . '</div>' ?>
</div>
