<?php

/**
 * Show detailed pod stats.
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';
global $totals;

?>
<div class="chart-container pe-auto d-flex">
    <canvas class="d-flex pe-auto" id="pod_chart_responses"></canvas>
</div>
<script>/**
 * Add a new chart for the passed data.
 *
 * @param id   HTML element ID to place the chart.
 * @param data Data to display on the chart.
 */
    new Chart(document.getElementById('pod_chart_responses'), {
        type: "line",
        data: {
            labels: <?php echo json_encode(array_column($totals, 'yymm')); ?>,
            datasets: [{
                data: <?php echo json_encode(array_column($totals, 'uptime')); ?>,
                label: '<?php echo $t->trans('base.general.uptime') ?>',
                fill: false,
                yAxisID: "l1",
                borderColor: "#A07614",
                backgroundColor: "#A07614)",
                borderWidth: 4,
                pointHoverRadius: 11
            }, {
                data: <?php echo json_encode(array_column($totals, 'latency')); ?>,
                label: '<?php echo $t->trans('base.general.latency') ?>',
                fill: false,
                yAxisID: "r1",
                borderColor: "#4b6588",
                backgroundColor: "#4b6588",
                borderWidth: 3,
                pointHoverRadius: 11,
                pointStyle: 'rect'
            }]
        },
        options: {
            responsive: true,
            interaction: {
                mode: 'index',
                intersect: false,
            },
            stacked: false,
            scales: {
                l1: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                    min: 50,
                    max: 100,
                },
                r1: {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    grid: {
                        drawOnChartArea: true,
                    },
                },
                x: {
                    ticks: {
                        maxRotation: 90,
                        minRotation: 90
                    }
                }
            }
        },
    })

</script>
