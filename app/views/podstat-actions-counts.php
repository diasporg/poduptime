<?php

/**
 * Show detailed pod stats.
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';
global $totals;

?>
<div class="chart-container pe-auto d-flex">
    <canvas class="d-flex pe-auto" id="pod_chart_counts"></canvas>
</div>
<script>
    /**
     * Add a new chart for the passed data.
     *
     * @param id   HTML element ID to place the chart.
     * @param data Data to display on the chart.
     */
    new Chart(document.getElementById('pod_chart_counts'), {
        type: "line",
        data: {
            labels: <?php echo json_encode(array_column($totals, 'yymm')); ?>,
            datasets: [{
                data: <?php echo json_encode(array_column($totals, 'local_posts')); ?>,
                label: '<?php echo $t->trans('base.general.posts') ?>',
                fill: false,
                yAxisID: "l2",
                borderColor: "#4b6588",
                backgroundColor: "#4b6588",
                borderWidth: 4,
                pointHoverRadius: 6
            }, {
                data: <?php echo json_encode(array_column($totals, 'comment_counts')); ?>,
                label: '<?php echo $t->trans('base.general.comments') ?>',
                fill: false,
                yAxisID: "l2",
                borderColor: "#cecaa7",
                backgroundColor: "#cecaa7",
                borderWidth: 4,
                pointHoverRadius: 6
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
                l2: {
                    position: "left"
                },
                x: {
                    ticks: {
                        maxRotation: 90,
                        minRotation: 90
                    }
                }
            },
            interaction: {
                intersect: false,
                mode: 'index',
            },
        }
    });
</script>
