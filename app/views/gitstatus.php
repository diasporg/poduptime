<?php

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

use RedBeanPHP\R;
use RedBeanPHP\RedException;

try {
    $versions = R::getAll('
        SELECT DISTINCT ON (software) software,
           version AS masterversion
        FROM masterversions
        ORDER BY software, id desc;
    ');
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

?>
    <table class="w-100 table table-striped table-responsive">
        <thead>
        <tr>
            <th scope="col"><?php echo $t->trans('base.strings.list.columns.software') ?></th>
            <th scope="col">masterversion</th>
            <th scope="col">gitsite</th>
            <th scope="col">repo</th>
            <th scope="col">url</th>
        </tr>
        </thead>
        <tbody>
<?php

foreach (c('softwares') as $software => $details) {
    if (!empty($details['gittype'])) {
        if ($details['gittype'] === 'github') {
            $releasejson = ('https://' . $details['gitsite'] . '/repos/' . $details['repo'] . '/releases/latest');
        } elseif ($details['gittype'] === 'gitlab') {
            $releasejson = ('https://' . $details['gitsite'] . '/api/v4/projects/' . $details['repo'] . '/repository/tags');
        } elseif ($details['gittype'] === 'gitea') {
            $releasejson = ('https://' . $details['gitsite'] . '/api/v1/repos/' . $details['repo'] . '/releases');
        }
        foreach ($versions as $version) {
            if ($software === $version['software']) {
                $v = $version['masterversion'];
            }
        }
        echo "<tr>";
        printf(
            '<td>%1$s</td><td>%2$s</td><td>%3$s</td><td>%4$s</td><td><a target="tab3" href="%5$s">%5$s</a></td>',
            $software,
            $v,
            $details['gitsite'],
            $details['repo'],
            $releasejson
        );
        echo "</tr>";
    }
}

echo "</tbody></table>";

