<?php

/**
 * Show map of pods.
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

$lat             = ipLocation($_SERVER['REMOTE_ADDR'])['lat'] ?? 31;
$long            = ipLocation($_SERVER['REMOTE_ADDR'])['long'] ?? -99;
$subdomain       = join('.', explode('.', $_SERVER['HTTP_HOST'], -2));
$software_all    = !empty($subdomain) ? ucwords($subdomain) : 'All';
$pods            = allServersList($subdomain);
$softwaren       = !empty($subdomain) ? '&software=' . $subdomain : '';

?>

<link rel="stylesheet" href="<?php echo $_SERVER['CDN_DOMAIN'] ?>/node_modules/leaflet/dist/leaflet.css"/>
<script src="<?php echo $_SERVER['CDN_DOMAIN'] ?>/node_modules/leaflet/dist/leaflet.js"></script>
<div class="container-fluid">
    <div class="row">
    <div class="col d-flex justify-content-center">
        <?php echo '<h2>' . $t->trans('base.strings.map.title') . ' ' . $software_all . '</h2>' ?>
    </div>
    </div>
</div>
<div class="shadow" id="map"></div>
<?php echo '<small>' . $t->trans('base.strings.map.tip')  . '</small>' ?>
<script type="text/javascript">
    var geoJsonData = {
        'type': 'FeatureCollection',
        'features': [
            <?php

            $i = 0;
            foreach ($pods as $pod) {
                $pod_name     = (preg_replace('/[^\p{L}\p{N} ]+/', '', $pod['domain']) ?? '');
                $pod_software = preg_replace('/[^\p{L}\p{N} ]+/', '', $pod['softwarename']) ?? '';
                $signup       = $pod['signup'] ? 'yes' : 'no';
                $pointlat     = $pod['long'] ?? null;
                $pointlong    = $pod['lat'] ?? null;
                if ($pointlat && $pointlong) {
                    $i++ > 0 && print ',';
                    if ($pod['greenhost']) {
                        echo <<<EOF
{
  'type': 'Feature',
  'id': '1',
  'properties' : {
    'html': '<div class="text-success">{$t->trans('base.strings.list.columns.greenhostdesc')}<br><a rel=”nofollow” href="/go&domain={$pod['domain']}">$pod_name</a><a href="/{$pod['domain']}"><img src="app/assets/images/info.svg" class="m-1" alt="Server Details" width="24" height="24"></a><br>{$t->trans('base.strings.list.columns.software')}: {$pod_software}<br>{$t->trans('base.strings.list.columns.signups')}: $signup<br>{$t->trans('base.strings.list.columns.months')}: {$pod['monthsmonitored']} <br>{$t->trans('base.strings.list.columns.users')}: {$pod['total_users']}<br>{$t->trans('base.strings.list.columns.uptime')}: {$pod['uptime_alltime']}%</div>',
            "style": {
            weight: 6,
            radius: 6,
            color: "#4CBB17",
            opacity: 1,
            fillColor: "#4CBB17",
            fillOpacity: 0.8
        }
  },
  'geometry': {
    'type': 'Point',
    'coordinates': [{$pointlat},{$pointlong}]
  }
}
EOF;
                    } else {
                        echo <<<EOF
{
  'type': 'Feature',
  'id': '1',
  'properties' : {
    'html': '<a rel=”nofollow” href="/go&domain={$pod['domain']}">$pod_name</a><a href="/{$pod['domain']}"><img src="app/assets/images/info.svg" class="m-1" alt="Server Details" width="24" height="24"></a><br>{$t->trans('base.strings.list.columns.software')}: {$pod_software}<br>{$t->trans('base.strings.list.columns.signups')}: $signup<br>{$t->trans('base.strings.list.columns.months')}: {$pod['monthsmonitored']} <br>{$t->trans('base.strings.list.columns.users')}: {$pod['total_users']}<br>{$t->trans('base.strings.list.columns.uptime')}: {$pod['uptime_alltime']}%',
            "style": {
            weight: 5,
            radius: 5,
            color: "#000",
            opacity: 0.5,
            fillColor: "#000",
            fillOpacity: 0.5
        }
  },
  'geometry': {
    'type': 'Point',
    'coordinates': [{$pointlat},{$pointlong}]
  }
}
EOF;
                    }
                }
            }
            ?>
        ]
    };
    const map = L.map('map', {zoom: 5, center: [<?php echo $lat; ?>, <?php echo $long; ?>]});

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 9,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    function onEachFeature(feature, layer) {
        if (feature.properties && feature.properties.html) {
            popupContent = feature.properties.html;
        }
        layer.bindPopup(popupContent);
    }

    const blacklayer = L.geoJSON(geoJsonData, {

        style(feature) {
            return feature.properties && feature.properties.style;
        },

        onEachFeature,

        pointToLayer(feature, latlng) {
            return L.circleMarker(latlng);
        }
    }).addTo(map);
</script>
<div class="text-success"><?php echo $t->trans('base.general.greenhost'); ?></div>
