<?php

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

?>
<script>
    function swapContent(page) {
        jQuery( "#adminDiv" ).append('Loading').load(page);
    }
</script>

<a href="#" onClick="return false" onmousedown="swapContent('/app/views/gitstatus.php');">Git Status</a>
<br>
<div id="adminDiv"></div>

