<?php

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

use Carbon\Carbon;
use RedBeanPHP\R;
use Nette\Utils\Arrays;

$allmeta = Arrays::associate(getAllMeta(), 'name');

$pods_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['pods_updated']['date_created'])->locale($locale->language)->diffInMinutes();
echo "<div class='m-4 p-4 w-auto shadow-lg'>";
if ($pods_updated_stats < $_SERVER['STATUS_GREEN']) {
    echo $t->trans('base.strings.status.current') . ' <div class="text-success d-inline-block">' . $t->trans('base.strings.status.green') . '</div> ' . $t->trans('base.strings.status.status') . ' ';
} else {
    echo $t->trans('base.strings.status.current') . ' <div class="text-danger d-inline-block">' . $t->trans('base.strings.status.red') . '</div> ' . $t->trans('base.strings.status.status') . ' ';
}
echo $allmeta['pods_updating']['value'] ? $t->trans('base.strings.status.running') : $t->trans('base.strings.status.idle');
echo "<br><br>";
$pods_updated_stats_human = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['pods_updated']['date_created'])->locale($locale->language)->diffForHumans();

echo $t->trans('base.strings.status.last') . ' ' . number_format(floatval(allDomainsData(null, true)[0]['count'])) . ' ' . $t->trans('base.general.servers') . '<br>';

echo $t->trans('base.strings.status.update') . ' '  . $pods_updated_stats_human . "<br>";

$last_runtime_human = secondsToTime($allmeta['pods_update_runtime']['value'] * 60);
echo $t->trans('base.strings.status.updatetook') . ' '  . $last_runtime_human . " <br>";

$back_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['backup_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.backup') . ' '  . $back_updated_stats . "<br>";

$dbcleanup = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['database_clean_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.dbcleanup') . ' '  . $dbcleanup . "<br>";

$dbvacuum = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['database_optimize_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.dbvacuum') . ' '  . $dbvacuum . "<br>";

$masterversions_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['masterversions_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.masterversion') . ' '  . $masterversions_updated_stats . "<br>";

$federation_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['federation_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.crawl') . ' '  . $federation_updated_stats . "<br>";

$rss = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['rssmap_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.rss') . ' '  . $rss . "<br>";

$sitemap = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['sitemap_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.sitemap') . ' '  . $sitemap . "<br>";

$apipush = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['pushapi_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.apipush', ['%(api)' => $_SERVER['PUSH_URL']]) . ' '  . $apipush . "<br>";

$statstable_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['statstable_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.monthly') . ' '  . $statstable_updated_stats . "<br>";

$statstable_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', $allmeta['daily_statstable_updated']['date_created'])->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.daily') . ' '  . $statstable_updated_stats . "<br>";

echo "<br>";

$gitBasePath   = '.git';
$gitStr        = file_get_contents($gitBasePath . '/HEAD');
$gitBranchName = rtrim(preg_replace("/(.*?\/){2}/", '', $gitStr));
$gitPathBranch = $gitBasePath . '/refs/heads/' . $gitBranchName;
$gitDate       = date('F j, Y', filemtime($gitPathBranch));

echo $t->trans('base.strings.status.running') . " <a href='https://gitlab.com/diasporg/poduptime'>Poduptime</a> ";
echo $t->trans('base.strings.status.poduptimeversion')  . " " . file_get_contents('tag.txt') . ", " . $gitDate . ", " . $t->trans('base.strings.status.branch') . " " . $gitBranchName;

echo "<br>";

try {
    $firstdate = R::getRow('
        SELECT date_created
        FROM servers
        WHERE id = ?
    ', ['2']);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}
try {
    $totalid = R::getRow('
        SELECT id
        FROM servers
        ORDER BY id desc 
        LIMIT 1
    ');
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

echo $t->trans('base.strings.status.first', ['%(location)' => $_SERVER['SERVER_LOCATION']]) . "  " . Carbon::createFromFormat('Y-m-d H:i:s', $firstdate['date_created'])->locale($locale->language)->diffForHumans() . " ";
echo $t->trans('base.strings.status.andtotal') . " " . number_format(floatval($totalid['id'])) . " " . $t->trans('base.general.servers');

echo "</div>";
