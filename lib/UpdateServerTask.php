<?php

/**
 * update a server from Async Task call
 */

declare(strict_types=1);

namespace Poduptime;

use Carbon\Carbon;
use DateTime;
use Exception;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use Spatie\Async\Task;
use tomverran\Robot\RobotsTxt;

class UpdateServerTask extends Task
{
    public function __construct(
        protected string $domain
    ) {
    }

    public function configure(): void
    {
        require_once __DIR__ . '/../boot.php';
    }

    public function run(): void
    {
        $debug       = $_SERVER['APP_DEBUG'] ?? false;
        $domain      = $this->domain;
        $pod = R::findOne('servers', 'domain = ?', [$domain]);

        if (!$pod) {
            podLog('Server not found on update call', $domain);
        }

        $score    = (int) $pod['score'];
        $nodeinfo = nodeInfo($domain, isset($_SERVER['PROCESS_TIMEOUT']) ? (int) $_SERVER['PROCESS_TIMEOUT'] : 22);
        if ($nodeinfo) {
            $pod['latency']    = round($nodeinfo['conntime'] + $nodeinfo['nstime'], 3);
            $pod['onion']      = parse_url($nodeinfo['header'][0]['onion-location'][0] ?? '', PHP_URL_HOST);
            $pod['i2p']        = parse_url($nodeinfo['header'][0]['x-i2p-location'][0] ?? '', PHP_URL_HOST);
            $pod['servertype'] = $nodeinfo['header'][0]['server'][0] ?? '';
            $debug && podLog('Nodeinfo URL ' . $nodeinfo['nodeinfourl'], $domain, 'debug');
            $debug && podLog('Nodeinfo Server Type ' . $pod['servertype'], $domain, 'debug');
            $debug && podLog('Nodeinfo service HTTP response code ' . $nodeinfo['httpcode'], $domain, 'debug');
            $debug && podLog('Latency ms ' . $pod['latency'], $domain, 'debug');
            $debug && podLog('Download Speed in kbps ' . $nodeinfo['downloadspeed'] * 0.008, $domain, 'debug');

            $jsonssl = json_decode($nodeinfo['body'] ?? null);
        }

        if (isset($jsonssl) && $nodeinfo['httpcode'] == 200) {
            $softwares = c('softwares');
            foreach ($softwares as $software => $details) {
                if ($software === $pod['softwarename']) {
                    $pod['pp']              = (empty($pod['pp']) && !empty($details['pp'])) ? 'https://' . $domain . $details['pp'] : $pod['pp'];
                    $pod['support']         = (empty($pod['support']) && !empty($details['support'])) ? 'https://' . $domain . $details['support'] : $pod['support'];
                    $pod['terms']           = (empty($pod['terms']) && !empty($details['terms'])) ? 'https://' . $domain . $details['terms'] : $pod['terms'];
                }
            }
            $pod['fullversion']           = $jsonssl->software->version ?? '0.0.0';
            preg_match_all('((?:\d+(.|-)?)+(\.)\d+\.*)', $pod['fullversion'], $sversion);
            $shortversion                 = $pod['shortversion'] = $sversion[0][0] ?? '0.0.0.0';
            $pod['signup']                = ($jsonssl->openRegistrations ?? false) === true;
            $swname                       = preg_replace('/[^a-z0-9 ]/i', '', strip_tags(strtolower($jsonssl->software->name ?? 'unknown')));
            $softwarename                 = $pod['softwarename'] = $swname;
            $pod['name']                  = $jsonssl->metadata->nodeName ?? $swname;
            $pod['total_users']           = $jsonssl->usage->users->total ?? '0';
            $pod['active_users_halfyear'] = $jsonssl->usage->users->activeHalfyear ?? '0';
            $pod['active_users_monthly']  = $jsonssl->usage->users->activeMonth ?? '0';
            $pod['local_posts']           = $jsonssl->usage->localPosts ?? '0';
            $pod['comment_counts']        = $jsonssl->usage->localComments ?? '0';
            $pod['owner']                 = $jsonssl->metadata->adminAccount ?? 'unknown';
            $pod['services']              = json_encode($jsonssl->services->outbound ?? null) ?? null;
            $pod['protocols']             = json_encode($jsonssl->protocols->outbound ?? $jsonssl->protocols ?? null) ?? null;
            $score += 1;

            writeCheck([$domain, true, null, $pod['latency'], $pod['total_users'], $pod['active_users_halfyear'], $pod['active_users_monthly'], $pod['local_posts'], $pod['comment_counts'], $shortversion, $pod['fullversion']]);

            $masterdata = R::getRow('
            SELECT version, devlastcommit, releasedate, date_checked
            FROM masterversions
            WHERE software = ?
            ORDER BY id
            DESC LIMIT 1
        ', [$softwarename]);

            $checks = R::getRow('
            SELECT
            round(avg(online::INT) * 100, 2) AS online,
            count (id) as total
            FROM checks
            WHERE domain = ?
            ', [$domain]);
            $totalchecks          = $checks['total'];
            $pod['checks_count']  = $totalchecks ?? $pod['checks_count'] ?? null;
            $debug && podlog('Server has been checked times', $pod['checks_count'], 'debug');

            $pod['masterversion'] = $masterdata['version'] ?? '0.0.0.0';
            $debug && podLog('Version code ' . $shortversion, $domain, 'debug');
            $debug && podLog('Masterversion ' . $pod['masterversion'], $domain, 'debug');
            $ipdata               = ipData($domain);
            $pod['ip']            = $ipdata['ipv4'][0] ?? null;
            $pod['dnssec']        = $ipdata['dnssec'];
            $pod['ipv6']          = $ipdata['ipv6'][0] ?? null;

            if ($pod['ip']) {
                $iplocaton = ipLocation($pod['ip']);
            } elseif ($pod['ipv6']) {
                $iplocaton = ipLocation($pod['ipv6']);
            }
            if ($pod['servertype'] == 'cloudflare') {
                $pod['countryname']  = 'Private';
                $pod['country']      = 'CF';
                $pod['city']         = 'Private';
                $pod['state']        = 'Private';
                $pod['lat']          = null;
                $pod['long']         = null;
                $pod['zipcode']      = null;
                $pod['metalocation'] = 'Hidden Behind CloudFlare';
            } else {
                $pod['countryname']  = $iplocaton['countryname'] ?? null;
                $pod['country']      = $iplocaton['country'] ?? null;
                $pod['city']         = $iplocaton['city'] ?? null;
                $pod['state']        = $iplocaton['state'] ?? null;
                $pod['lat']          = isset($iplocaton['lat']) ? substr_replace((string) $iplocaton['lat'], (string) rand(115, 740), -3, 3) : null;
                $pod['long']         = isset($iplocaton['long']) ? substr_replace((string) $iplocaton['long'], (string) rand(165, 999), -3, 3) : null;
                $pod['zipcode']      = $iplocaton['zipcode'] ?? null;
                $pod['metalocation'] = $iplocaton['metalocation'] ?? null;
            }
            try {
                $diff = new DateTime()->diff(new DateTime($pod['date_created']));
            } catch (Exception $e) {
                podLog('Error in Datetime ' . $e->getMessage(), $domain, 'error');
            }
            $pod['monthsmonitored'] = $diff->m + ($diff->y * 12);
            $pod['daysmonitored']   = $diff->days;
            $pod['uptime_alltime']  = $checks['online'] ?? null;

            $debug && podLog('Location meta ' . $pod['metalocation'], $domain, 'debug');
            $debug && podLog('IPv4 ' . $pod['ip'], $domain, 'debug');
            $debug && podLog('IPv6 ' . $pod['ipv6'], $domain, 'debug');
            $debug && podLog('Signup Open ' . $pod['signup'], $domain, 'debug');
            $debug && podLog('Uptime ' . $pod['uptime_alltime'], $domain, 'debug');

            $langhours    = Carbon::createFromFormat('Y-m-d H:i:s', $pod['date_lastlanguage'])->diffInHours() ?? 24;
            $debug && podLog('Last Language Check hours ago ' . $langhours, $domain, 'debug');
            $html_snippet = null;
            if (($langhours > ($_SERVER['LANGUAGE_REFRESH'] ?? 24)) || $totalchecks <= 1) {
                $robotsTxt = new RobotsTxt(curl("https://$domain/robots.txt", false, 9)['body']);
                if ($robotsTxt->isAllowed($_SERVER['USERAGENT'], '/')) {
                    $debug && podLog('Allowed by Robots ', $domain, 'debug');
                    $html_snippet = getWebsiteSnippetFromUrl("https://$domain/");
                } else {
                    $debug && podLog('Disallowed by Robots ', $domain, 'debug');
                }
                if (isset($html_snippet['body'])) {
                    $pod['detectedlanguage'] = detectWebsiteLanguageFromSnippet($html_snippet['body']) ?? $pod['detectedlanguage'];
                    $pod['metatitle'] = $html_snippet['title'] ?? $pod['metatitle'] ?? '';
                    $pod['metadescription'] = $html_snippet['description'] ?? $pod['metadescription'] ?? '';
                    $pod['metaimage'] = $html_snippet['image'] ?? $pod['metaimage'] ?? '';
                    $debug && podLog('Detected Language ' . $pod['detectedlanguage'], $domain, 'debug');
                    $debug && podLog('Meta Tag Title ' . $pod['metatitle'], $domain, 'debug');
                    $debug && podLog('Meta Tag Description ' . $pod['metadescription'], $domain, 'debug');
                    $debug && podLog('Meta Image ' . $pod['metaimage'], $domain, 'debug');
                }

                $pod['greenwebdata'] = isGreenHost($domain);
                $pod['greenhost'] = json_decode($pod['greenwebdata'])->green;
                $debug && podLog('GreenHost ' . $pod['greenhost'], $domain, 'debug');

                $pod['host'] = ipHost($pod['ip'] ?? $pod['ipv6']);
                $debug && podLog('Host ' . $pod['host'], $domain, 'debug');
                $pod['date_lastlanguage'] = date('Y-m-d H:i:s');
            }
            $pod['status'] = PodStatus::UP;
        }

        if ($nodeinfo['httpcode'] !== 200) {
            $debug && podLog('Can not connect to server error: ' . $nodeinfo['httpcode'], $domain, 'debug');
            writeCheck([$domain, false, 'Can not connect to server error: ' . $nodeinfo['httpcode']]);
            $score -= 2;
            $pod['status'] = PodStatus::DOWN;
        }

        if ($score == ($pod['podmin_notify_level'] - 1) && $pod['podmin_notify'] && $_SERVER['APP_ENV'] == 'production' && (int) $pod['score'] == $pod['podmin_notify_level']) {
            $message = 'Notice for ' . $domain . '. Your score is ' . $score . ' and your server will fall off the list soon.';
            sendEmail($pod['email'], 'Monitoring Notice from', $message, $_SERVER['ADMIN_EMAIL']);
        }

        if ($score > 100) {
            $score = 100;
        }

        if ($score == $_SERVER['MIN_SCORE']) {
            $pod['date_diedoff'] = date('Y-m-d H:i:s');
            $pod['status'] = PodStatus::SYSTEM_DELETED;
            addHistory($domain, 'deletion', 'score ' . $score);
        }

        $debug && podLog('Score ' . $score, $domain, 'debug');
        $debug && podLog('Status ' . $pod['status'], $domain, 'debug');
        try {
            $pod['date_laststats'] = date('Y-m-d H:i:s');
            $pod['score']          = $score;
            $id                    = R::store($pod);
            R::close();
            podLog('server data stored, record:' . $id, $domain);
        } catch (RedException $e) {
            podLog('Error in SQL query ' . $e->getMessage(), $domain, 'error');
        }
    }
}
