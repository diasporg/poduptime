<?php

/**
 * add a server from Async Task call
 */

declare(strict_types=1);

namespace Poduptime;

use Exception;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use Spatie\Async\Task;

class AddServerTask extends Task
{
    public $output;
    public function __construct(
        protected string $domain,
        protected string $note
    ) {
    }

    public function configure()
    {
        require_once __DIR__ . '/../boot.php';
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        $publickey       = $_SERVER['DOMAIN'] . "-site-verification=" . bin2hex(random_bytes(9));
        $domain          = cleanDomain($this->domain);

        if (checkNodeinfo($domain)) {
            try {
                $p                        = R::dispense('servers');
                $p['domain']              = $domain;
                $p['softwarename']        = 'detecting';
                $p['date_lastlanguage']   = date('Y-m-d H:i:s');
                $p['date_created']        = date('Y-m-d H:i:s');
                $p['date_updated']        = date('Y-m-d H:i:s');
                $p['publickey']           = $publickey;
                $this->output             = R::store($p);
                podLog('server added to database, record:' . $this->output, $domain);
                addHistory($domain, 'addition', $this->note);
                updateServer($domain);
            } catch (RedException $e) {
                $_SERVER['APP_DEBUG'] && podLog('server not added database' . $e, $domain, 'error');
            }
        } else {
            $_SERVER['APP_DEBUG'] && podLog('adding server failed', $domain, 'error');
        }
    }
}
