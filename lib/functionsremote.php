<?php

/**
 * Collection of functions that make remote calls to other servers.
 */

declare(strict_types=1);

use DCarbone\CURLHeaderExtractor;
use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader\InvalidDatabaseException;
use Nette\Utils\Arrays;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use GeoIp2\Database\Reader as GeoReader;
use DetectLanguage\DetectLanguage;
use Symfony\Component\Process\Process;
use Laminas\Validator\Ip;
use DOM\HTMLDocument;
use MaxMind\Db\Reader;

/**
 * Execute cURL request and return array of data.
 */
function curl(string $url, bool $follow = false, int $timeout = 30, string $token = null, bool $json = true, bool $post = false, string $data = null): array
{
    $chss = curl_init();

    if ($token) {
        curl_setopt($chss, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer $token"
        ]);
    } elseif ($token && $json) {
        curl_setopt($chss, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer $token",
            "Content-Type: application/json"
        ]);
    } elseif ($json) {
        curl_setopt($chss, CURLOPT_HTTPHEADER, [
            "Content-Type: application/json"
        ]);
    }

    curl_setopt($chss, CURLOPT_URL, $url);
    if ($post) {
        curl_setopt($chss, CURLOPT_POST, true);
        curl_setopt($chss, CURLOPT_POSTFIELDS, $data);
    } else {
        curl_setopt($chss, CURLOPT_HTTPGET, true);
    }
    curl_setopt($chss, CURLOPT_DNS_SERVERS, ($_SERVER['DNS_SERVER'] ?? '1.1.1.1') . ',' . ($_SERVER['DNS_SERVER_BACKUP'] ?? '1.0.0.1'));
    curl_setopt($chss, CURLOPT_CONNECTTIMEOUT, (int) $timeout);
    curl_setopt($chss, CURLOPT_TIMEOUT, (int) $timeout);
    curl_setopt($chss, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($chss, CURLOPT_CERTINFO, true);
    curl_setopt($chss, CURLOPT_ENCODING, '');
    curl_setopt($chss, CURLOPT_TCP_FASTOPEN, '1L');
    curl_setopt($chss, CURLOPT_MAXCONNECTS, (int) $_SERVER['PROCESS_LIMIT'] ?: 50);
    curl_setopt($chss, CURLOPT_SSL_FALSESTART, false);
    curl_setopt($chss, CURLOPT_MAXREDIRS, 5);
    curl_setopt($chss, CURLOPT_FOLLOWLOCATION, $follow);
    curl_setopt($chss, CURLOPT_USERAGENT, $_SERVER['USERAGENT']);
    curl_setopt($chss, CURLOPT_HEADER, true);

    $exec = curl_exec($chss);
    if (is_string($exec)) {
        list($headers, $body) = CURLHeaderExtractor::getHeaderAndBody($exec);
    } else {
        $headers = $exec;
        $body    = '';
    }

    $data = [
        'body'           => $body,
        'headers'        => $headers,
        'error'          => curl_error($chss),
        'info'           => curl_getinfo($chss, CURLINFO_CERTINFO),
        'code'           => curl_getinfo($chss, CURLINFO_RESPONSE_CODE),
        'conntime'       => curl_getinfo($chss, CURLINFO_CONNECT_TIME),
        'nstime'         => curl_getinfo($chss, CURLINFO_NAMELOOKUP_TIME),
        'downloadspeed'  => curl_getinfo($chss, CURLINFO_SPEED_DOWNLOAD_T),
        'totaltime'      => curl_getinfo($chss, CURLINFO_TOTAL_TIME)
    ];

    curl_close($chss);

    return $data;
}

/**
 * update all git data from list in config file
 */
function masterVersionCrawl(): bool
{
    foreach (c('softwares') as $software => $details) {
        if (!empty($details['gittype'])) {
            $m = R::dispense('masterversions');
            if ($details['gittype'] === 'github') {
                $releasejson = curl('https://api.github.com/repos/' . $details['repo'] . '/releases/latest');
                if ($releasejson['code'] === 200) {
                    $json = json_decode($releasejson['body']);
                    $masterversion = isset($json->tag_name) ? str_replace('v', '', $json->tag_name) : '';
                    $m['releasedate'] = $json->published_at ?? '';
                } else {
                    $m['releasedate'] = date('Y-m-d H:i:s');
                    $masterversion    = 'e.r.r';
                }
            } elseif ($details['gittype'] === 'gitlab') {
                $releasejson = curl('https://' . $details['gitsite'] . '/api/v4/projects/' . $details['repo'] . '/repository/tags');
                if ($releasejson['code'] === 200) {
                    $json = json_decode($releasejson['body']);
                    $masterversion = isset($json[0]->name) ? str_replace('v', '', $json[0]->name) : '';
                    $m['releasedate'] = $json[0]->commit->created_at ?? '';
                } else {
                    $m['releasedate'] = date('Y-m-d H:i:s');
                    $masterversion    = 'e.r.r';
                }
            } elseif ($details['gittype'] === 'gitea') {
                $releasejson = curl('https://' . $details['gitsite'] . '/api/v1/repos/' . $details['repo'] . '/releases');
                if ($releasejson['code'] === 200) {
                    $json = json_decode($releasejson['body']);
                    $masterversion = isset($json[0]->tag_name) ? str_replace('v', '', $json[0]->tag_name) : '';
                    $m['releasedate'] = $json[0]->created_at ?? '';
                } else {
                    $m['releasedate'] = date('Y-m-d H:i:s');
                    $masterversion    = 'e.r.r';
                }
            }
            $m['software'] = $software;
            $m['version']  = preg_replace('/-.*$/', '', $masterversion ?? '');
            if ($m['releasedate'] && $m['version']) {
                try {
                    R::store($m);
                    podLog('masterversion data store success ' . $m['version'], $software);
                } catch (RedException $e) {
                    podLog('Error in SQL query ' . $e->getMessage(), $software, 'error');
                }
            } else {
                podLog('masterversion data store failed http code ' . $releasejson['code'], $software);
            }
        }
    }
    return true;
}

/**
 * Helper to extract pods from cURL response.
 */
function extractPodsFromCurl(array $curl, ?string $field = null, ?string $column = null, ?string $array = null): array
{
    try {
        $pods = json_decode($curl['body'] ?: '', true, 512, JSON_THROW_ON_ERROR);
    } catch (JsonException) {
        return [];
    }

    if (!$field && !$column) {
        return $pods;
    }

    if ($array) {
        $pods = $pods[$array];
    }

    if ($field) {
        $pods = $pods[$field];
    }

    if ($column) {
        return array_column($pods, $column);
    }

    return $pods;
}

/**
 * Check for internet connection
 */
function isConnected(): bool
{
    return (bool) @fsockopen('icanhazip.com', 80);
}

/**
 * get IP datas
 */
function ipData($domain): array
{
    $dnsserver        = $_SERVER['DNS_SERVER'] ?: '1.1.1.1';
    $dnsserver_backup = $_SERVER['DNS_SERVER_BACKUP'] ?: '1.0.0.1';

    $ipData = [
        'dnssec' => false,
    ];

    $lookups = [
        'ipv4' => 'A',
        'ipv6' => 'AAAA',
        'txt'  => 'TXT',
    ];

    foreach ($lookups as $key => $lookup) {
            @//todo dig can do idn but Process seems to fail out, this works for now
            $dig = new Process(['dig', "@$dnsserver", idn_to_ascii($domain), '+dnssec', $lookup]);
            $dig->setTimeout((int) $_SERVER['PROCESS_TIMEOUT'] ?: 30);
            $dig->run();
        if (!$dig->isSuccessful()) {
            $dig = new Process(['dig', "@$dnsserver_backup", idn_to_ascii($domain), '+dnssec', $lookup]);
            $dig->setTimeout((int) $_SERVER['PROCESS_TIMEOUT'] ?: 30);
            $dig->run();
        }
            $lookupData       = explode(PHP_EOL, trim($dig->getOutput()));
            $ipData['dnssec'] = $ipData['dnssec'] || preg_grep('/;; flags:.*ad.*;/', $lookupData);
            $lookupValues     = preg_grep('/\s+IN\s+' . $lookup . '\s+.*/', $lookupData);
        foreach ($lookupValues as $lookupValue) {
            preg_match('/' . $lookup . '\s(.*)/', $lookupValue, $match);
            $ipData[$key][] = $match[1];
        }
    }
    return $ipData;
}


/**
 * get IP location datas
 */
function ipLocation($ip): array
{
    $validator = new Ip();
    if (!$validator->isValid($ip)) {
        return [];
    }

    try {
        $reader = new GeoReader($_SERVER['GEOLITE_CITY']);
        $geo = $reader->city($ip);
    } catch (InvalidDatabaseException | AddressNotFoundException) {
        return [];
    }
    //could be improved using the languages that come back, different for every IP
    $metalocation  = $geo->country->names['en'] ?? null;
    $metalocation .= $geo->city->names['en'] ?? null;
    $metalocation .= $geo->continent->names['en'] ?? null;
    $metalocation .= $geo->subdivisions['0']->names['en'] ?? null;

    return [
        'countryname'  => $geo->country->name ?? null,
        'country'      => $geo->country->isoCode ?? null,
        'city'         => $geo->city->name ?? null,
        'state'        => $geo->mostSpecificSubdivision->name ?? null,
        'lat'          => $geo->location->latitude ?? null,
        'long'         => $geo->location->longitude ?? null,
        'zipcode'      => $geo->postal->code ?? null,
        'metalocation' => $metalocation
    ];
}

/**
 * get IP asn name
 */
function ipHost($ip)
{
    $validator = new Ip();
    if (!$validator->isValid($ip)) {
        return [];
    }

    try {
        $reader = new Reader($_SERVER['IP_ASN_MMDB']);
        $asn    = $reader->get($ip)['name'];
    } catch (InvalidDatabaseException) {
        return [];
    }

    return $asn;
}

/**
 * fetch and parse nodeinfo
 */
function nodeInfo($domain, $timeout = 5): ?array
{
    $nodeinfo_meta = curl("https://$domain/.well-known/nodeinfo", false, (int) $timeout);

    $nodeinfourl = "https://$domain/nodeinfo/1.0";

    if (array_key_exists('links', json_decode($nodeinfo_meta['body'], true) ?? [])) {
        if ($info = json_decode($nodeinfo_meta['body'] ?: '', true)) {
            $links = Arrays::get($info, 'links', '[]');
            foreach ($links as $urls) {
                if ($urls['rel'] == 'http://nodeinfo.diaspora.software/ns/schema/2.1') {
                    $nodeinfourl = $urls['href'];
                } elseif ($urls['rel'] == 'http://nodeinfo.diaspora.software/ns/schema/2.0') {
                    $nodeinfourl = $urls['href'];
                } elseif ($urls['rel'] == 'http://nodeinfo.diaspora.software/ns/schema/1.1') {
                    $nodeinfourl = $urls['href'];
                } elseif ($urls['rel'] == 'http://nodeinfo.diaspora.software/ns/schema/1.0') {
                    $nodeinfourl = $urls['href'];
                }
            }
        }
    }

    if (parse_url($nodeinfourl, PHP_URL_HOST) == $domain or parse_url($nodeinfourl, PHP_URL_HOST) == idn_to_ascii($domain)) {
        $nodeinfo = curl($nodeinfourl, false, (int) $timeout);
        return [
            'nodeinfourl'   => $nodeinfourl,
            'body'          => $nodeinfo['body'],
            'sslerror'      => $nodeinfo['error'],
            'info'          => $nodeinfo['info'],
            'httpcode'      => $nodeinfo['code'],
            'conntime'      => $nodeinfo['conntime'],
            'downloadspeed' => $nodeinfo['downloadspeed'],
            'totaltime'     => $nodeinfo['totaltime'],
            'header'        => $nodeinfo['headers'],
            'nstime'        => $nodeinfo['nstime'],
        ];
    } else {
        return null;
    }
}

/**
 * Get a language snippet from a given URL.
 */
function getWebsiteSnippetFromUrl(string $url): ?array
{
    $curl = curl($url, true, (int) $_SERVER['PROCESS_TIMEOUT'] ?? 30, null, false);

    if (!$curl['body']) {
        return null;
    }
    if ($curl['code'] !== 200) {
        return null;
    }

    $d           = HTMLDocument::createFromString($curl['body'], LIBXML_NOERROR);
    $title       = trim($d->title ?? '', " \n\r\t\v\0") ?? '';
    $body        = $title;
    $description = '';
    $image       = '';

    foreach ($d->getElementsByTagName('meta') as $meta) {
        if (strtolower($meta->getAttribute('name') ?? '') == 'description') {
            $body        .= ' ' . mb_strimwidth($meta->getAttribute('content'), 0, 255);
            $description  = mb_strimwidth($meta->getAttribute('content'), 0, 255);
        }
        if (strtolower($meta->getAttribute('property') ?? '') == 'og:image') {
            $image = $meta->getAttribute('content');
        }
    }

    return array_map('strip_tags', compact('body', 'title', 'description', 'image'));
}

/**
 * Detect the language of the given text snippet.
 */
function detectWebsiteLanguageFromSnippet(string $snippet): ?string
{
    return DetectLanguage::simpleDetect($snippet);
}

/**
 * check https://admin.thegreenwebfoundation.org api for hosting data
 */
function isGreenHost(string $domain): string
{
    $greenjson = curl('https://admin.thegreenwebfoundation.org/api/v3/greencheck/' . $domain);
    return $greenjson['body'];
}
