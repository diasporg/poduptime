<?php

/**
 * Collection of functions for local use.
 */

declare(strict_types=1);

use Laminas\Validator\IsJsonString;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\MemoryPeakUsageProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\WebProcessor;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;
use Poduptime\AddServerTask;
use Poduptime\PodStatus;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use Spatie\Async\Pool;
use Spatie\DbDumper\Compressors\GzipCompressor;
use Spatie\DbDumper\Databases\PostgreSql;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use CodeZero\BrowserLocale\BrowserLocale;

/**
* log things: type = debug, info, notice, warning, error, critical, alert, emergency
 */
function podLog($text, $meta = null, $type = 'info')
{
    $CrawlerDetect = new CrawlerDetect();
    if (!$CrawlerDetect->isCrawler()) {
        $log = new Logger('poduptime');
        $uptype = strtoupper($type);
        if (!isCli()) {
            $log->pushHandler(new StreamHandler($_SERVER['LOG_DIRECTORY'] . '/user-' . $_SERVER['APP_ENV'] . '.log', "$uptype"));
            $log->pushProcessor(new WebProcessor());
            $browser  = new BrowserLocale($_SERVER["HTTP_ACCEPT_LANGUAGE"] ?? 'en');
            $language = $browser->getLocale()->language;
            $log->$type($text, ['meta' => $meta, 'UA' => $_SERVER['HTTP_USER_AGENT'] ?? '', 'LANG' => $language ?? '']);
        } else {
            $log->pushHandler(new StreamHandler($_SERVER['LOG_DIRECTORY'] . '/script-' . $_SERVER['APP_ENV'] . '.log', "$uptype"));
            $log->pushProcessor(new MemoryUsageProcessor());
            $log->pushProcessor(new MemoryPeakUsageProcessor());
            $log->$type($text, ['meta' => $meta]);
        }
    }
}

/**
 * Helper to get config array values.
 */
function c(string $param = null, mixed $default = null): mixed
{
    static $config;
    if ($config === null) {
        $config = require $_SERVER['APP_CONFIG'] ?? __DIR__ . '/../config/app.php';
        is_array($config) || die('Invalid config format.');
    }

    if ($param === null) {
        return $config;
    }

    if (array_key_exists($param, $config)) {
        return $config[$param];
    }

    return $default;
}

/**
 * Check if started from cli.
 */
function isCli(): bool
{
    global $ci;
    if ($ci) {
        return false;
    }
    if (defined('STDIN')) {
        return true;
    }

    if (PHP_SAPI === 'cli') {
        return true;
    }

    if (PHP_SAPI === 'cgi-fcgi') {
        return true;
    }

    if (array_key_exists('SHELL', $_SERVER)) {
        return true;
    }

    if (empty($_SERVER['REMOTE_ADDR']) && !isset($_SERVER['HTTP_USER_AGENT']) && count($_SERVER['argv']) > 0) {
        return true;
    }

    if (!array_key_exists('REQUEST_METHOD', $_SERVER)) {
        return true;
    }

    return false;
}

/**
 * Update meta table
 *
 */
function addMeta(string $name, mixed $value = '1'): int|string
{
    try {
        $u          = R::dispense('meta');
        $u['name']  = $name;
        $u['value'] = $value;
        return R::store($u);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
}

/**
 * Update history table
 *
 */
function addHistory(string $domain, string $change, string $note): int|string
{
    try {
        $u          = R::dispense('history');
        $u['domain']       = $domain;
        $u['change']       = $change;
        $u['note']         = $note;
        return R::store($u);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
}

/**
 * Update server data
 *
 */
function writeServer(string $domain, string $key, mixed $value): mixed
{
    try {
        $server        = R::findOne('servers', 'domain = ?', [$domain]);
        $server[$key]  = $value;
        return R::store($server);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
}

/**
 * Update server data
 *
 */
function readServer(string $domain, string|null $key): mixed
{
    $server = R::findOne('servers', 'domain = ?', [$domain]);
    if ($key) {
        return $server[$key];
    } else {
        return $server;
    }
}

/**
 * Get meta table last item
 */
function getMeta(string $name, string $column = 'value'): mixed
{
    return R::getCell("
        SELECT $column
        FROM meta
        WHERE name = ?
        ORDER BY id DESC
        LIMIT 1
    ", [$name]);
}

/**
 * Get meta table last item for all %update%
 */
function getAllMeta(): mixed
{
    return R::getAll("
        SELECT DISTINCT ON (name) name, date_created, value
        FROM meta
        WHERE name LIKE '%updat%'
        GROUP BY name, date_created, value
        ORDER BY name, date_created DESC
    ");
}

/**
 * Clean old data from database
 */
function cleanDatabase($days = 90): bool
{
    $success = R::exec("
       DELETE FROM checks 
       WHERE domain IN (SELECT domain FROM servers WHERE score <= ?)
       AND date_checked < NOW() - INTERVAL '$days days' 
    ", [$_SERVER['MIN_SCORE']]);
    if ($success == 1) {
        return true;
    } else {
        return false;
    }
}

/**
 * Optimise tables
 */
function optimiseDatabase($full = false): bool
{
    if ($full) {
        R::exec("
        VACUUM FULL checks
    ");
    } else {
        R::exec("
        VACUUM checks
    ");
    }

    $success = true;
    return $success;
}

/**
 * Format Seconds to human for last update
 */
function secondsToTime(int $seconds, $format = '%3$d hours, %2$d minutes'): string
{
    $seconds = (int) abs($seconds);
    $minutes = floor($seconds / 60);
    $hours   = floor($minutes / 60);

    return sprintf($format, $seconds % 60, $minutes % 60, $hours);
}

/**
 * Convert text file to proper sql like query
 */
function txtToQuery(string $file): string
{
    return '%(' . preg_replace('/\R+/', "|", trim(file_get_contents($file))) . ')%';
}

/**
 * backup sql data to drive
 */
function backupData(): string
{
    $backup_file = $_SERVER['BACKUP_DIRECTORY'] . '/poduptime_tar_dump_' . $_SERVER['APP_ENV'] . '.' . date('Y.m.d.His') . '.sql.gz';

    PostgreSql::create()
    ->setDbName($_SERVER['DB_DATABASE'])
    ->setPort((int) $_SERVER['DB_PORT'])
    ->setUserName($_SERVER['DB_USERNAME'])
    ->setPassword($_SERVER['DB_PASSWORD'])
    ->addExtraOption('--clean')
    ->addExtraOption('--no-owner')
    ->addExtraOption('--format=tar')
    ->useCompressor(new GzipCompressor())
    ->dumpToFile($backup_file);

    //remove old backups
    foreach (
        Finder::findFiles('poduptime_tar_dump_*.sql.gz')
        ->date('<=', $_SERVER['BACKUP_RETENTION'])
        ->from($_SERVER['BACKUP_DIRECTORY']) as $file
    ) {
        try {
            FileSystem::delete($file->getRealPath());
            podLog('Old Backup Deleted', $file->getRealPath());
        } catch (Nette\IOException $e) {
            podlog('Error Deleting Backupfile', $e, 'error');
        }
    }

    return $backup_file;
}

/**
 * write to checks table from input array
 */
function writeCheck($array): bool
{
    try {
        $c                           = R::dispense('checks');
        $c['domain']                 = $array[0] ?? null;
        $c['online']                 = $array[1] ?? null;
        $c['error']                  = $array[2] ?? null;
        $c['latency']                = $array[3] ?? null;
        $c['total_users']            = $array[4] ?? null;
        $c['active_users_halfyear']  = $array[5] ?? null;
        $c['active_users_monthly']   = $array[6] ?? null;
        $c['local_posts']            = $array[7] ?? null;
        $c['comment_counts']         = $array[8] ?? null;
        $c['shortversion']           = $array[9] ?? null;
        $c['version']                = $array[10] ?? null;
        R::store($c);
        $success = true;
    } catch (RedException $e) {
        podLog('Error in SQL query' . $e->getMessage());
        $success = false;
    }
    return $success;
}

/**
 * read from checks table
 */
function readCheck($domain, $online): array
{
    $checkdata = R::findOne('checks', 'domain = ? AND online = ?', [$domain, $online]);

    return [
        'date_checked' => $checkdata['date_checked'] ?? null,
        'version'      => $checkdata['version'] ?? null,
    ];
}

/**
* send email using sendmail from system install
 **/
function sendEmail($to, $subject, $message, $cc = ''): bool
{
    $mail = new PHPMailer(true);

    try {
        $mail->isSMTP();
        $mail->Host       = $_SERVER['SMTP_HOSTNAME'];
        $mail->SMTPAuth   = true;
        $mail->Username   = $_SERVER['SMTP_USERNAME'];
        $mail->Password   = $_SERVER['SMTP_PASSWORD'];
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->Port       = $_SERVER['SMTP_PORT'] ?? 587;
        $mail->CharSet    = 'UTF-8';
        $mail->setFrom($_SERVER['ADMIN_EMAIL'], $_SERVER['TITLE']);
        $mail->addAddress($to);
        $dcc              = ($cc !== '') ? $cc : $_SERVER['ADMIN_EMAIL'];
        $mail->addCC($dcc);
        $mail->isHTML(true);
        $mail->Subject    = $subject . ' ' . $_SERVER['TITLE'];
        $mail->Body       = $message;
        $mail->AltBody    = $message;
        $sent             = $mail->send();
    } catch (Exception $e) {
        podLog("Message could not be sent. Mailer Error" . $mail->ErrorInfo . $e, $to, "error");
    }
    return $sent;
}

/**
 * add one server
 */
function addServer(string $domain, $note = ''): bool
{
    $pool = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);
    $pool
        ->add(new AddServerTask(cleanDomain($domain), $note))
        ->catch(fn() => false);
    $pool->forceSynchronous()->wait();
    return true;
}

/**
 * list of servers data for tables and maps et
 */
function allServersList($software = null, $latest = null, $hours = null)
{
    $hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
    $hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

    $sql = '
        SELECT domain, dnssec, podmin_statement, masterversion, shortversion, softwarename, daysmonitored, monthsmonitored, score, signup, protocols, name, country, countryname, city, state, detectedlanguage, uptime_alltime, active_users_halfyear, active_users_monthly, services, latency, date_updated, ipv6, total_users, local_posts, comment_counts, status, date_laststats, lat, long, date_created, metatitle, metadescription, greenhost
    FROM servers
    WHERE status < ? 
    AND score > 0
    AND softwarename NOT SIMILAR TO ?
    AND softwarename != \'detecting\'
    AND domain NOT SIMILAR TO ?
';
    if ($software) {
        $sql .= ' AND softwarename = ? ORDER BY score DESC';
        $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains, $software]);
    } elseif ($latest && $hours) {
        $sql .= " AND date_created > current_date - interval '$hours hours' ORDER BY date_created DESC";
        $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains]);
    } elseif ($latest) {
        $sql .= " AND score = 51 AND date_created > current_date - interval '2 hours' ORDER BY date_created DESC";
        $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains]);
    } else {
        $sql .= ' ORDER BY score DESC';
        $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains]);
    }
    return $pods;
}

/**
 * list of domains for other queries
 */
function allDomainsData($software = null, $count = false, $dead = false)
{
    $hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
    $hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);
    if ($count) {
        $sql = '
        SELECT count(id)
        ';
        if ($software) {
            $sql .= ' FROM servers WHERE status <= ? AND softwarename = ? AND softwarename NOT SIMILAR TO ? AND domain NOT SIMILAR TO ?';
            $pods = R::getAll($sql, [PodStatus::RECHECK, $software, $hiddensoftwares, $hiddendomains]);
        } elseif ($dead) {
            $sql .= ' FROM servers';
            $pods = R::getAll($sql);
        } else {
            $sql .= ' FROM servers WHERE status <= ? AND softwarename NOT SIMILAR TO ? AND domain NOT SIMILAR TO ?';
            $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains]);
        }
    } else {
        $sql = '
        SELECT domain
        ';
        if ($software) {
            $sql .= ' FROM servers WHERE status <= ? AND softwarename = ? AND softwarename NOT SIMILAR TO ? AND domain NOT SIMILAR TO ? ORDER BY id';
            $pods = R::getAll($sql, [PodStatus::RECHECK, $software, $hiddensoftwares, $hiddendomains]);
        } elseif ($dead) {
            $sql .= ' FROM servers ORDER BY id';
            $pods = R::getAll($sql);
        } else {
            $sql .= ' FROM servers WHERE status <= ? AND softwarename NOT SIMILAR TO ? AND domain NOT SIMILAR TO ? ORDER BY id';
            $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains]);
        }
    }

    return $pods;
}

function oneDomainsData($domain)
{
    $hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
    $hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);
    try {
        $server = R::getRow('
        SELECT *
        FROM servers
        WHERE domain = ?
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
    ', [$domain, $hiddensoftwares, $hiddendomains]);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
    return $server;
}

function oneDomainsChecks($domain)
{
    $sql = "SELECT
        to_char(date_checked, 'yyyy MM') AS yymm,
        count(*) AS total_checks,
        round(avg(online::INT),4)*100 AS uptime,
        round(avg(latency),2) * 1000 AS latency,
        round(avg(total_users)) AS users,
        round(avg(local_posts)) AS local_posts,
        round(avg(comment_counts)) AS comment_counts,
        round(avg(active_users_halfyear)) AS active_users_halfyear,
        round(avg(active_users_monthly)) AS active_users_monthly
    FROM checks
    WHERE domain = ?
    GROUP BY yymm
    ORDER BY yymm";
    $checks = R::getAll($sql, [$domain]);
    return $checks;
}

function oneDomainsClicks($domain)
{
    $domain_clicks = R::getAll("
    SELECT
        to_char(date_clicked, 'yyyy-mm') AS yymm,
        SUM(manualclick) AS manualclick, 
        SUM(autoclick) AS autoclick
    FROM clicks
    WHERE domain = ?
    GROUP BY yymm
    ORDER BY yymm
", [$domain]);
    return $domain_clicks;
}

/**
 * find closest servers by geo
 */
function closestServers($ip = null, $limit = 5, $software = '%')
{
    $iploc = ipLocation($ip ?? $_SERVER['REMOTE_ADDR']);

    $sql = "
    SELECT greenhost, domain, softwarename, city, state, countryname,
    ST_Distance(ST_MakePoint(long::numeric, lat::numeric)::geography, ST_MakePoint(:long, :lat)::geography) AS postgis_distance,
    earth_distance(ll_to_earth(:lat, :long), ll_to_earth (lat::numeric, long::numeric)) AS earth_distance,
    (point(long::numeric, lat::numeric) <@> point(:long, :lat)) * 1609.344 AS point_distance
    FROM
    servers
    WHERE
    score = 100
    AND status < :status
    AND monthsmonitored > 5
    AND uptime_alltime > 99
    AND signup = 'true'
    AND total_users > 1
    AND softwarename SIMILAR TO :software
    AND softwarename NOT SIMILAR TO :hiddensoftwares
    AND domain NOT SIMILAR TO :hiddendomains
    AND earth_box(ll_to_earth (:lat, :long), 5000000) @> ll_to_earth (lat::numeric, long::numeric)
    AND earth_distance(ll_to_earth (:lat, :long), ll_to_earth (lat::numeric, long::numeric)) < 5000000
    ORDER BY
    earth_distance, monthsmonitored, uptime_alltime
    LIMIT :limit
    ";
    $hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
    $hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

    $closest = R::getAll($sql, [
        ':status' => PodStatus::RECHECK,
        ':hiddensoftwares' => $hiddensoftwares,
        ':software' => $software,
        ':hiddendomains' => $hiddendomains,
        ':lat' => $iploc['lat'],
        ':long' => $iploc['long'],
        ':limit' => $limit
    ]);
    return $closest;
}

/**
 * Validate nodeinfo on domain
 */
function checkNodeinfo(string $domain): bool
{
    $validator = new IsJsonString();
    $nodejson  = nodeInfo($domain) !== null ? nodeInfo($domain)['body'] : '';
    //@todo this should also check your numbers are legit somehow since people are faking numbers
    if ($validator->isValid($nodejson)) {
        if (stripos($nodejson, 'openRegistrations')) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * Clean a domain name up
 */
function cleanDomain(string $domain): string
{
        $domain = idn_to_utf8($domain);
        $domain = preg_replace('/[\/@:\\/]/', '', strval($domain));
    if ($domain) {
        $domain = strtolower(htmlspecialchars($domain));
    } else {
        $domain = "invalid";
    }
    return $domain;
}

/**
 * resort an array by key
 * @param $array
 * @param $key
 * @return array
 */
function groupBy($array, $key): array
{
    $return = array();
    foreach ($array as $val) {
        $return[$val[$key]][] = $val;
    }
    return $return;
}

function toast($text): string
{
    $toast = "
    <script>
    window.addEventListener('load', function () {
    const toastLive = document.getElementById('liveToast')
    var toastdiv = document.getElementById('42')
    toastdiv.replaceWith('$text')
    const toast = new bootstrap.Toast(toastLive)
    toast.show()
   })
    </script>
    ";
    return $toast;
}

/**
 * change MAINTENANCE_MODE env setting
 */
function changeMM(string $setting = 'false')
{
    if (file_exists('.env')) {
        exec("sed -i 's|MAINTENANCE_MODE=.*|MAINTENANCE_MODE=${setting}|' .env");
        return true;
    } else {
        return false;
    }
}
