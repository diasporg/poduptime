<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

class MonthlyStats extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('monthlystats');
        $table
            ->addColumn('softwarename', 'text')
            ->addColumn('total_users', 'integer')
            ->addColumn('total_active_users_halfyear', 'integer')
            ->addColumn('total_active_users_monthly', 'integer')
            ->addColumn('total_posts', 'integer')
            ->addColumn('total_comments', 'integer')
            ->addColumn('total_pods', 'integer')
            ->addColumn('total_uptime', 'integer')
            ->addColumn('date_checked', 'timestamp', ['default' => 'current_timestamp'])
            ->create();
    }
}
