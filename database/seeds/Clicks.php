<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

class Clicks extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('clicks');
        $table
            ->addColumn('domain', 'text')
            ->addColumn('manualclick', 'integer')
            ->addColumn('autoclick', 'integer')
            ->addColumn('date_clicked', 'timestamp', ['default' => 'current_timestamp'])
            ->create();
    }
}
