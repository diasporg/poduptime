<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

class ZzData extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $data = [
            [
                'name'    => 'languages_updated',
            ],[
                'name'    => 'geoip_updated',
            ],[
                'name'    => 'masterversions_updated',
            ],[
                'name'    => 'federation_updated',
            ],[
                'name'    => 'statstable_updated',
            ],[
                'name'    => 'daily_statstable_updated',
            ],[
                'name'    => 'backup_updated',
            ],[
                'name'    => 'database_clean_updated',
            ],[
                'name'    => 'database_optimize_updated',
            ],[
                'name'    => 'pods_updated',
            ],[
                'name'    => 'sitemap_updated',
            ],[
                'name'    => 'rssmap_updated',
            ],[
                'name'    => 'pushapi_updated',
            ]
        ];

        $posts = $this->table('meta');
        $posts->insert($data)
            ->saveData();

        $this->execute('CREATE EXTENSION IF NOT EXISTS cube CASCADE');
        $this->execute('CREATE EXTENSION IF NOT EXISTS postgis CASCADE');
        $this->execute('CREATE EXTENSION IF NOT EXISTS earthdistance CASCADE');
    }
}
