<?php

declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

class Servers extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('pods');
        $table
            ->addColumn('domain', 'text', ['null' => false])
            ->addColumn('name', 'text')
            ->addColumn('metatitle', 'text')
            ->addColumn('metadescription', 'text')
            ->addColumn('metanodeinfo', 'text')
            ->addColumn('metalocation', 'text')
            ->addColumn('metaimage', 'text')
            ->addColumn('owner', 'text')
            ->addColumn('onion', 'text')
            ->addColumn('i2p', 'text')
            ->addColumn('terms', 'text')
            ->addColumn('pp', 'text')
            ->addColumn('support', 'text')
            ->addColumn('camo', 'text')
            ->addColumn('zipcode', 'text')
            ->addColumn('softwarename', 'text')
            ->addColumn('masterversion', 'text')
            ->addColumn('fullversion', 'text')
            ->addColumn('shortversion', 'text')
            ->addColumn('score', 'integer', ['default' => 50])
            ->addColumn('ip', 'text')
            ->addColumn('detectedlanguage', 'text')
            ->addColumn('country', 'text')
            ->addColumn('countryname', 'text')
            ->addColumn('city', 'text')
            ->addColumn('state', 'text')
            ->addColumn('lat', 'text')
            ->addColumn('long', 'text')
            ->addColumn('email', 'text')
            ->addColumn('ipv6', 'text')
            ->addColumn('sslvalid', 'text')
            ->addColumn('monthsmonitored', 'integer')
            ->addColumn('daysmonitored', 'integer')
            ->addColumn('signup', 'boolean')
            ->addColumn('total_users', 'integer')
            ->addColumn('active_users_halfyear', 'integer')
            ->addColumn('active_users_monthly', 'integer')
            ->addColumn('local_posts', 'integer')
            ->addColumn('uptime_alltime', 'decimal', ['precision' => 5, 'scale' => 2])
            ->addColumn('status', 'smallinteger', ['default' => 1])
            ->addColumn('latency', 'decimal', ['precision' => 8, 'scale' => 6])
            ->addColumn('services', 'jsonb')
            ->addColumn('protocols', 'jsonb')
            ->addColumn('additional_meta', 'jsonb')
            ->addColumn('greenwebdata', 'jsonb')
            ->addColumn('greenhost', 'boolean')
            ->addColumn('host', 'text')
            ->addColumn('token', 'text')
            ->addColumn('publickey', 'text')
            ->addColumn('tokenexpire', 'timestamp')
            ->addColumn('podmin_statement', 'text')
            ->addColumn('podmin_notify', 'boolean')
            ->addColumn('podmin_notify_level', 'integer', ['default' => 90])
            ->addColumn('sslexpire', 'timestamp')
            ->addColumn('dnssec', 'boolean')
            ->addColumn('servertype', 'text')
            ->addColumn('comment_counts', 'integer')
            ->addColumn('checks_count', 'integer')
            ->addColumn('date_updated', 'timestamp', ['default' => 'current_timestamp'])
            ->addColumn('date_laststats', 'timestamp', ['default' => 'current_timestamp'])
            ->addColumn('date_created', 'timestamp', ['default' => 'current_timestamp'])
            ->addColumn('date_diedoff', 'timestamp')
            ->addColumn('date_lastlanguage', 'timestamp', ['default' => 'current_timestamp'])
            ->create();
    }
}
