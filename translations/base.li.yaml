---
base:
  general:
    yes: 'Yes'
    no: 'No'
    up: Up
    down: Down
    close: Close
    next: Next
    back: Back
    server: Server
    servers: Servers
    submit: Submit
    save: Save
    delete: Delete
    pause: Pause
    unpause: Unpause
    error: Error
    date: Date
    notice: Notice
    users: Users
    comments: Comments
    posts: Posts
    uptime: Uptime
    latency: Latency
    manual: Manual
    auto: Auto
    moreinfo: Detailed information about server
    visitserver: Visit this
    greenhost: Servers in green are identified as eco hosted by the Green Web Foundation
    api: You can find more details about this server using our API, we have limited details here
  navs:
    list: List
    map: Map
    stats: Stats
    monthlystats: Monthly
    months: Months
    dailystats: Daily
    days: Days
    software: Software Filter
    all: all
    search: Search
    add: Add
    edit: Edit
    admin: Admin
    source: Source
    api: API
    updated: Updated
    status: Status
    translation: Translate
    terms: Terms
    history: History
  strings:
    about: '%(sitename) finds all servers in the fediverse and gives you an easy way to find a home'
    history:
      title: History of server changes
      domain: Domain
      change: Change
      note: Note
      datechanged: Date
      tip: Recent changes to servers.
    list:
      navs:
        auto: Auto Pick
        autotip: One click find you the best %(software) server
      columns:
        title: List view of
        server: Server
        serverdesc: A server is a site for you to set up your account
        name: Name
        namedesc: Defined name of this server
        version: Version
        versiondesc: Version of software this server runs
        software: Software
        softwaredesc: Type of fediverse software this server runs
        uptime: Uptime
        uptimedesc: Percent of the time the server is online
        latency: Latency
        latencydesc: Average connection latency in ms from %(hostlocation)
        signups: Signups
        signupsdesc: Does this server allow new users
        users: Users
        activeusers: Active Users
        usersdesc: Number of total users on this server
        users6: 6m
        users6desc: Number of users active last 6 months on this server
        users1: 1m
        users1desc: Number of users active last 1 month on this server
        posts: Posts
        postsdesc: Number of total posts on this server
        comments: Comments
        commentsdesc: Number of total comments on this server
        months: Months
        monthsdesc: How many months have we been watching this server
        score: Score
        scoredesc: System Score on a 100 point scale
        status: Status
        statusdesc: System Status
        country: Country
        countrydesc: Server country, based on IP Geolocation
        city: City
        citydesc: Server city, based on IP Geolocation
        state: State
        statedesc: Server state, based on IP Geolocation
        language: Language
        languagedesc: Server language auto-detected from their main page data
        greenhost: GreenHost
        greenhostdesc: Green Web Foundation identifies this as a green hosted server
    map:
      title: Map view of
      tip: Use the software menu selector to limit this data.
    search:
      software: Software
      language: Language
      location: Location
      open: Open for Signups
      results: '%(number) results for query: %(searchterm)'
      nextpage: Next Page
      lastpage: Last Page
      pagenumber: Page Number
      greenhost: GreenHost
    stats:
      title: Entire Fediverse
      users: Users by Software
      serverss: Servers by Software
      serversc: Servers by Country
      serversh: Servers by Host
      serversgreen: Green Servers
      statsfor: Monthly stats for
      average: Average
      total: Total
      growth: Total Users by Month
      growthactive: Active Users by Month
      serversper: Servers Online by Month
      halfyear: Active Users Halfyear
      monthly: Active Users Monthly
      commentsper: Comments by Month
      postsper: Posts by Month
      tip: Pie charts are top 20. All graphs are averaged monthly data for servers that choose to share data to the public. Use the software selector on menu to change these charts to just one software vs all.
    dailystats:
      title: Entire Fediverse
      users: Users by Software
      serverss: Servers by Software
      serversc: Servers by Country
      serversh: Servers by Host
      serversgreen: Green Servers
      statsfor: Daily stats for
      average: Average
      total: Total
      growth: Total Users by Day
      growthactive: Active Users by Day
      serversper: Servers Online by Day
      commentsper: Comments by Day
      postsper: Posts by Day
      tip: Pie charts are top 20. All graphs are averaged daily data for servers that choose to share data to the public. Use the software selector on menu to change these charts to just one software vs all.
    singlepage:
      uptime: Uptime & Speed
      userstats: User Stats
      actionstats: Action Stats
      clicksout: Clicks Out
      opensignup: This server is allowing new users to sign up
      closedsignup: This server is not accepting new users
      lastchecked: Server last checked
      notfound: Fediverse server not found
      addit: You can always add it
      deletedview: View deleted server information
      private: This server uses a CDN to block its actual location, you should investigate where the actual server is located before using
      about: About
      charts: Charts
      data: Main Data
      metadata: Additional Meta Data
      errors: Errors
      deleted: Server has been deleted
      findother: We can help you find a new %(software) server
      goerror: Could not auto pick for you, no servers look good enough. Try the list or map to pick a server manually.
      green: This website using a GreenHost
      name: Server Name
      metatitle: Server Title
      metadescription: Description
      detectedlanguage: Detected Language
      ip: IP
      ipv6: IPv6
      greenhost: GreenHost
      host: Server Host
      dnssec: DNSSEC
      servertype: HTTP Server
      terms: Terms
      pp: Privacy Policy
      support: Support
      softwarename: Software Name
      shortversion: Server Version
      masterversion: Repository Version
      daysmonitored: Days Monitored
      date_laststats: Last Checked
      date_created: Date Added
      countryname: Country
      city: City
      state: State
      zipcode: Zip Code
      uptime_alltime: Uptime Percent
      latency: Latency
      signup: Signups Open
      podmin_statement: Server Owner Statement
    status:
      current: System status is
      red: Red
      green: Green
      status: and currently
      idle: Idle
      running: Running
      last: Last Update scanned
      update: Last Update was
      updatetook: Last Update took
      language: Last Language check was
      backup: Last Data Backup was
      dbcleanup: Last Data Cleanup was
      dbvacuum: Last Data Vacuum was
      apipush: Last Push to API %(api) was
      rss: Last RSS file update was
      sitemap: Last sitemap update was
      masterversion: Last Masterversion update from git repos was
      crawl: Last Fediverse crawl was
      monthly: Last Monthly stats table update was
      daily: Last Daily stats table update was
      poduptimeversion: Software Version
      branch: Branch
      first: This site located at %(location), found its first server
      andtotal: and discovered a total of
    allsoftwares:
      title: All known softwares sorted by active users
welcome:
  main:
    find: Let's find you a fediverse home!
    suggested: Use a suggested %(software) server close to you
    picked: '%(domain) is a %(software) server in %(location)'
    nopicked: No good servers near %(location) for %(software) were found, use list or map to find more
    first: Or, Filter sites by software
    second: Then, Filter sites by view
    map: Use a <b class="fw-bold">map</b> to find a server close to you. <br>This will make things faster for you!
    list: Use a <b class="fw-bold">list</b> of servers to find a home. <br>You can filter and sort on the top of the list.
softwares:
  diaspora: Diaspora is a privacy-aware, distributed, open source social network
  friendica: Friendica is a decentralised communications platform that integrates social communication
  hubzilla: Hubzilla is a powerful platform for creating interconnected websites featuring a decentralized identity
  pleroma: Pleroma is a free, federated social networking server built on open protocols
  socialhome: Socialhome is best described as a federated personal profile with social networking functionality
  social-relay: A relay system for the federation not for end use
  writefreely: WriteFreely is free and open source software for building a writing space on the web
  ganggo: GangGo is a decentralized social network written in GoLang
  funkwhale: Funkwhale is a community-driven project that lets you listen and share music and audio within a decentralized, open network
  osada: Osada is a conversational style macroblogging network powered by hyper-drive social engine, supporting ActivityPub and Zot6 protocols
  mastodon: Mastodon is an open source decentralized social network - by the people for the people
  pixelfed: Pixelfed is a free and ethical photo sharing platform, powered by ActivityPub federation
  wordpress: Wordpress is open source software which you can use to easily create a beautiful website, blog, or app
  misskey: Misskey is a decentralized microblogging platform born on Earth
  speechmore: Speechmore is the social network that leaves you in control of your data and gives you the freedom to say whatever you have to
  peertube: PeerTube is a free and open-source, decentralized, federated video platform powered by ActivityPub and WebTorrent
  plume: Plume is a federated blogging application
  rustodon: Rustodon is an Mastodon-compatible federated social microblogging server
  microblogpub: Microblogpub is a self-hosted, single-user, ActivityPub powered microblog
  mobilizon: Mobilizon is a tool designed to create platforms for managing communities and events
  lemmy: Lemmy is an open-source, easily self-hostable link aggregator that you can use to share
  gnusocial: GNU social is the eldest free social networking platform for public and private communications used in federated social networks
  gotosocial: GoToSocial provides a lightweight, customizable, and safety-focused entryway into the Fediverse, and is comparable to (but distinct from) existing projects such as Mastodon, Pleroma, Friendica, and PixelFed.
  ecko: Ecko is a community-driven fork of Mastodons social network software. The idea for the fork is to optimize toward community, that is making it as easy as possible to contribute
  bookwyrm: BookWyrm is a platform for social reading! You can use it to track what youre reading, review books, and follow your friends
  akkoma: a smallish microblogging platform, aka the cooler pleroma
  calckey: Calckey is based off of Misskey, a powerful microblogging server on ActivityPub with features such as emoji reactions, a customizable web ui, rich chatting, and much more!
  drupal: With robust content management tools, sophisticated APIs for multichannel publishing, and a track record of continuous innovation—Drupal is the best digital experience platform(DXP) on the web
  epicyon: Epicyon is a fediverse server suitable for self-hosting a small number of accounts on low power systems
  foundkey: FoundKey is a free and open source microblogging server compatible with ActivityPub. Forked from Misskey, FoundKey improves on maintainability and behaviour, while also bringing in useful features
  gancio: a shared agenda for local communities (with activitypub support)
  ktistec: Ktiste is an ActivityPub server. It is intended for individual users
  owncast: Owncast is an open source, self-hosted, decentralized, single user live video streaming and chat server for running your own live streams similar in style to the large mainstream options
  firefish: Firefish is based off of Misskey, a powerful microblogging server on ActivityPub with features such as emoji reactions, a customizable web UI, rich chatting, and much more!
  kbin: kbin is an open source reddit-like content aggregator and microblogging platform for the fediverse.
  birdsitelive: BirdsiteLIVE is a Twitter to ActivityPub bridge
  fedibird: Fedibird is a free, open-source social network server based on ActivityPub, which is a fork of Mastodon
  notestock: ActivityPub Support SNS (Mastodon, Misskey, Pleroma, etc) posts are posted.
  mbin: a federated content aggregator, voting, discussion and microblogging platform (By the community, for the community)
  cherrypick: CherryPick is an open source, decentralized social media platform that's free forever!
  meisskey: Powered by Misskey
  sharkey: Sharkey is an open source, decentralized social media platform that's free forever!
  hajkey: Hajkey is yet another fork of Misskey, bringing you no-nonsense fixes, features & improvements you actually want since 2023
  iceshrimp: Iceshrimp is a decentralized and federated social networking service, implementing the ActivityPub standard
  glitchcafe: Decentralized social media powered by Mastodon
admin:
  add: You can add or edit a server you own here
  domain: Server Domain
  domainnote: The base domain name of your server (without trailing slash)
  email: Your Email (optional)
  emailnote: We'll never share your email with anyone else
  nodomain: no server domain given
  dupeserver: Server already exists and is registered to an owner, use the edit function to modify
  dnsrecord: Server already exists, you can claim this domain by adding a DNS TXT record that states
  dnsnote: Refresh this page after updating your DNS records to be able to add your email address to this server
  emailsuccess: Email added to domain
  emailmissing: Go back and enter the email you want to use on the form
  addemailsubject: New Server Added to
  addemailbody: New server https://%(domain) added to database. New servers start with a score of 50, give it some time to be checked and show up.
  addsuccess: Data successfully inserted! Your server will be checked and live on the list in a few hours!
  error: Could not validate your server, check your setup!<br>Take a look at your <a href="//%(domain)/.well-known/nodeinfo">nodeinfo</a>
  edit: Want to update your server?
  notoken: no token given
  noserver: no server domain given
  badtoken: bad token
  domainnotfound: domain not found
  mismatch: token mismatch
  expired: token expired
  deleted: server deleted
  systemdeleted: Server has been system deleted, no updates can be made, it can not be reversed.
  paused: server paused
  unpaused: server unpaused
  editemailsubject: Edit notice from
  editemailbody: Data for %(domain) updated. Will be checked on next server update, see /status page.
  editsuccess: Data saved
  authorized: Authorized to edit <a href="https://%(server)/%(domain)">%(domain)</a> for %(hours)
  emailrequired: Email
  termsurl: URL to terms
  ppurl: URL to Privacy Policy
  supporturl: 'URL to Support(use mailto: if email address)'
  statement: Admin Statement (500 character limit)
  notify: Notify by email if server fails checks
  notifylevel: Notify when your score falls to
  status: Your server status is currently
  delete: Stop checking and Stop showing your server. Can be re-enabled later.
  pause: Stop checking your server. Can be re-enabled later.
  unpause: Unpause/Undelete - Start checking and Start showing your server.
  tokenemailsubject: Edit Key for
  tokenemailbody: This link %(expires)
  tokensuccess: Link send to registered email
  register: This server does not have an email on file, use the <a href="/podmin">add a server link</a> to register your server.
