---
base:
  general:
    yes: 'Ja'
    no: 'Nei'
    up: Opp
    down: Ned
    close: Lukk
    next: Neste
    back: Tilbake
    server: Server
    servers: Servere
    submit: Sende inn
    save: Lagre
    delete: Slett
    pause: Pause
    unpause: Gjenoppta pause
    error: Feil
    date: Dato
    notice: Legge merke til
    users: Brukere
    comments: Kommentarer
    posts: Innlegg
    uptime: Oppetid
    latency: Ventetid
    manual: Håndbok
    auto: Auto
    moreinfo: Detaljert informasjon om serveren
    visitserver: Besøk denne
    greenhost: Servere i grønt er identifisert som øko vert av Green Web Foundation
    api: Du finner flere detaljer om denne serveren ved hjelp av API-et vårt, vi har begrenset informasjon her
  navs:
    list: Liste
    map: Kart
    stats: Statistikk
    monthlystats: Månedlig
    months: Måneder
    dailystats: Daglig
    days: Dager
    software: Programvarefilter
    all: alle
    search: Søk
    add: Legge til
    edit: Redigere
    admin: Admin
    source: Kilde
    api: API
    updated: Oppdatert
    status: Status
    translation: Oversette
    terms: Vilkår
    history: Historikk
  strings:
    about: '%(sitename) finner alle servere i fediverse og gir deg en enkel måte å finne et hjem på'
    history:
      title: Historikk for serverendringer
      domain: Domene
      change: Endre
      note: Notat
      datechanged: Dato
      tip: Nylig endringer i servere.
    list:
      navs:
        auto: Autovalg
        autotip: Ett klikk finner deg den beste %(software) serveren
      columns:
        title: Listevisning av
        server: Server
        serverdesc: En server er et nettsted der du kan sette opp kontoen din
        name: Navn
        namedesc: Definert navn på denne serveren
        version: Versjon
        versiondesc: Versjon av programvare denne serveren kjører
        software: Programvare
        softwaredesc: Type Fediverse-programvare som denne serveren kjører
        uptime: Oppetid
        uptimedesc: Prosent av tiden serveren er online
        latency: Ventetid
        latencydesc: Gjennomsnittlig tilkoblingsforsinkelse i ms fra %(hostlocation)
        signups: Påmeldinger
        signupsdesc: Tillater denne serveren nye brukere
        users: Brukere
        activeusers: Aktive brukere
        usersdesc: Antall brukere totalt på denne serveren
        users6: 6m
        users6desc: Antall brukere som har vært aktive siste 6 måneder på denne serveren
        users1: 1m
        users1desc: Antall brukere som har vært aktive siste 1 måned på denne serveren
        posts: Innlegg
        postsdesc: Antall totalt innlegg på denne serveren
        comments: Kommentarer
        commentsdesc: Antall totalt kommentarer på denne serveren
        months: Måneder
        monthsdesc: Hvor mange måneder har vi sett på denne serveren
        score: Score
        scoredesc: Systemscore på en 100 poengs skala
        status: Status
        statusdesc: System status
        country: Land
        countrydesc: Serverland, basert på IP Geolocation
        city: By
        citydesc: Serverby, basert på IP Geolocation
        state: Stat
        statedesc: Serverstatus, basert på IP Geolocation
        language: Språk
        languagedesc: Serverspråk oppdaget automatisk fra hovedsidedataene deres
        greenhost: GreenHost
        greenhostdesc: Green Web Foundation identifiserer dette som en grønn vertsserver
    map:
      title: Kartvisning av
      tip: Bruk programvaremenyvelgeren for å begrense disse dataene.
    search:
      software: Programvare
      language: Språk
      location: plassering
      open: Åpent for påmeldinger
      results: '%(number) resultater for spørringen: %(searchterm)'
      nextpage: Neste side
      lastpage: Siste side
      pagenumber: Side nummer
      greenhost: GreenHost
    stats:
      title: Hele Fediverse
      users: Brukere etter programvare
      serverss: Servere etter programvare
      serversc: Servere etter land
      serversh: Servere etter vert
      serversgreen: Grønne servere
      statsfor: Månedlig statistikk for
      average: Gjennomsnitt
      total: Totalt
      growth: Totalt antall brukere etter måned
      growthactive: Aktive brukere etter måned
      serversper: Servere online etter måned
      halfyear: Aktive brukere halvår
      monthly: Aktive brukere månedlig
      commentsper: Kommentarer etter måned
      postsper: Innlegg etter måned
      tip: Sektor diagrammer er øverst 20. Alle grafer gjennomsnittlig månedlig for servere som velger å dele data til publikum. Bruke programvarevelgeren på menyen for å endre disse kartene til bare én programvare versus alle.
    dailystats:
      title: Hele Fediverse
      users: Brukere etter programvare
      serverss: Servere etter programvare
      serversc: Servere etter land
      serversh: Servere etter vert
      serversgreen: Grønne servere
      statsfor: Daglig statistikk for
      average: Gjennomsnitt
      total: Totalt
      growth: Totalt antall brukere per dag
      growthactive: Aktive brukere etter dag
      serversper: Servere online etter dag
      commentsper: Kommentarer etter dag
      postsper: Innlegg etter dag
      tip: Sektor diagrammer er øverst 20. Alle grafer gjennomsnittlig daglige data for servere som velger å dele data til publikum. Bruke programvarevelgeren på menyen for å endre disse kartene til bare én programvare versus alle.
    singlepage:
      uptime: Oppetid og hastighet
      userstats: Brukerstatistikk
      actionstats: Handlingsstatistikk
      clicksout: Klikker ut
      opensignup: Denne serveren lar nye brukere registrere seg
      closedsignup: Denne serveren godtar ikke nye brukere
      lastchecked: Server sist sjekket
      notfound: Fediverse-server ble ikke funnet
      addit: Du kan alltid legge den til
      deletedview: Se slettet serverinformasjon
      private: Denne serveren bruker et CDN for å blokkere den faktiske plasseringen, du bør undersøke hvor den faktiske serveren befinner seg før du bruker den
      about: Om
      charts: Diagrammer
      data: Hoveddata
      metadata: Ytterligere metadata
      errors: Feil
      deleted: Serveren er slettet
      findother: Vi kan hjelpe deg med å finne en ny %(software) -server
      goerror: Kunne ikke velge automatisk for deg, ingen servere ser gode nok ut. Prøv listen eller kartet for å velge en server manuelt.
      green: Denne nettsiden bruker en GreenHost
      name: Servernavn
      metatitle: Servertittel
      metadescription: Beskrivelse
      detectedlanguage: Oppdaget språk
      ip: IP
      ipv6: IPv6
      greenhost: GreenHost
      host: Serververt
      dnssec: DNSSEC
      servertype: HTTP-server
      terms: Vilkår
      pp: Personvernerklæring
      support: Støtte
      softwarename: Programvarenavn
      shortversion: Serverversjon
      masterversion: Repository-versjon
      daysmonitored: Dager overvåket
      date_laststats: Sist sjekket
      date_created: Dato lagt til
      countryname: Land
      city: By
      state: Tilstand
      zipcode: Postnummer
      uptime_alltime: Prosent oppetid
      latency: Latens
      signup: Påmeldinger åpne
      podmin_statement: Servereiererklæring
    status:
      current: Systemstatus er
      red: rød
      green: Grønn
      status: og for øyeblikket
      idle: Tomgang
      running: Løping
      last: Siste oppdatering skannet
      update: Siste oppdatering var
      updatetook: Siste oppdatering tok
      language: Siste språksjekk var
      backup: Siste sikkerhetskopiering av data var
      dbcleanup: Siste dataopprydding var
      dbvacuum: Siste datavakuum var
      apipush: Siste Push til API %(api) var
      rss: Siste RSS-filoppdatering var
      sitemap: Siste nettstedskartoppdatering var
      masterversion: Siste Masterversion-oppdatering fra git repos var
      crawl: Siste Fediverse-gjennomgang var
      monthly: Siste månedlige statistikktabelloppdatering var
      daily: Siste daglige statistikktabelloppdatering var
      poduptimeversion: Programvareversjon
      branch: gren
      first: Denne siden som ligger på %(location), fant sin første server
      andtotal: og oppdaget totalt
    allsoftwares:
      title: All kjent programvare sortert etter aktive brukere
welcome:
  main:
    find: La oss finne et uhyggelig hjem til deg!
    suggested: Bruk en foreslått %(software) server nær deg
    picked: '%(domain) er en %(software) server i %(location)'
    nopicked: Ingen gode servere i nærheten av %(location) for %(software) ble funnet, bruk liste eller kart for å finne flere
    first: Eller, filtrer nettsteder etter programvare
    second: Deretter filtrerer du nettsteder etter visning
    map: Bruk et <b class="fw-bold">-kart</b> for å finne en server nær deg. <br>Dette vil gjøre ting raskere for deg!
    list: Bruk en <b class="fw-bold">liste</b> over servere for å finne et hjem. <br>Du kan filtrere og sortere på toppen av listen.
softwares:
  diaspora: Diaspora er et personvernbevisst, distribuert, åpen kildekode sosialt nettverk
  friendica: Friendica er en desentralisert kommunikasjonsplattform som integrerer sosial kommunikasjon
  hubzilla: Hubzilla er en kraftig plattform for å lage sammenkoblede nettsteder med en desentralisert identitet
  pleroma: Pleroma er en gratis, forent sosial nettverksserver bygget på åpne protokoller
  socialhome: Socialhome beskrives best som en forent personlig profil med funksjonalitet for sosiale nettverk
  social-relay: Et relésystem for forbundet ikke for sluttbruk
  writefreely: WriteFreely er gratis og åpen kildekode-programvare for å bygge et skriveområde på nettet
  ganggo: GangGo er et desentralisert sosialt nettverk skrevet i GoLang
  funkwhale: Funkwhale er et fellesskapsdrevet prosjekt som lar deg lytte og dele musikk og lyd i et desentralisert, åpent nettverk
  osada: Osada er et makrobloggnettverk i samtalestil drevet av en sosial motor med hyperdrive, som støtter ActivityPub og Zot6-protokoller
  mastodon: Mastodon er et åpen kildekode desentralisert sosialt nettverk - av folket for folket
  pixelfed: Pixelfed er en gratis og etisk bildedelingsplattform, drevet av ActivityPub-forbundet
  wordpress: Wordpress er åpen kildekode-programvare som du enkelt kan bruke til å lage en vakker nettside, blogg eller app
  misskey: Misskey er en desentralisert mikrobloggplattform født på jorden
  speechmore: Speechmore er det sosiale nettverket som gir deg kontroll over dataene dine og gir deg friheten til å si hva du måtte
  peertube: PeerTube er en gratis og åpen kildekode, desentralisert, forent videoplattform drevet av ActivityPub og WebTorrent
  plume: Plume er en forent bloggapplikasjon
  rustodon: Rustodon er en Mastodon-kompatibel forent sosial mikrobloggserver
  microblogpub: Microblogpub er en egenvert, en-bruker, ActivityPub-drevet mikroblogg
  mobilizon: Mobilizon er et verktøy utviklet for å lage plattformer for å administrere fellesskap og arrangementer
  lemmy: Lemmy er en åpen kildekode, enkelt selv-hostbar lenkeaggregator som du kan bruke til å dele
  gnusocial: GNU social er den eldste gratis sosiale nettverksplattformen for offentlig og privat kommunikasjon som brukes i forente sosiale nettverk
  gotosocial: GoToSocial gir en lett, tilpassbar og sikkerhetsfokusert inngang til Fediverse, og er sammenlignbar med (men forskjellig fra) eksisterende prosjekter som Mastodon, Pleroma, Friendica og PixelFed.
  ecko: Ecko er en fellesskapsdrevet gaffel av Mastodons sosiale nettverksprogramvare. Ideen med gaffelen er å optimalisere mot fellesskapet, det vil si å gjøre det så enkelt som mulig å bidra
  bookwyrm: BookWyrm er en plattform for sosial lesing! Du kan bruke den til å spore hva du leser, anmelde bøker og følge vennene dine
  akkoma: en liten mikrobloggplattform, også kalt den kulere pleroma
  calckey: Calckey er basert på Misskey, en kraftig mikrobloggserver på ActivityPub med funksjoner som emoji-reaksjoner, en tilpassbar web-UI, rik chatting og mye mer!
  drupal: Med robuste innholdsadministrasjonsverktøy, sofistikerte API-er for flerkanalspublisering og en merittliste for kontinuerlig innovasjon – Drupal er den beste digitale opplevelsesplattformen (DXP) på nettet
  epicyon: Epicyon er en fediverse-server som egner seg for å være vert for et lite antall kontoer på lavstrømssystemer
  foundkey: FoundKey er en gratis mikrobloggserver med åpen kildekode som er kompatibel med ActivityPub. Forked from Misskey, FoundKey forbedrer vedlikeholdsevnen og oppførselen, samtidig som den bringer inn nyttige funksjoner
  gancio: en delt agenda for lokalsamfunn (med aktivitetspubstøtte)
  ktistec: Ktiste er en ActivityPub-server. Den er beregnet på individuelle brukere
  owncast: Owncast er en åpen kildekode, selvdrevet, desentralisert, enkeltbruker live videostreaming og chat-server for å kjøre dine egne live-strømmer i stil med de store mainstream-alternativene
  firefish: Firefish er basert på Misskey, en kraftig mikrobloggserver på ActivityPub med funksjoner som emoji-reaksjoner, et tilpassbart nettgrensesnitt, rik chat og mye mer!
  kbin: kbin er en åpen kildekode reddit-lignende innholdsaggregator og mikrobloggplattform for fediverse.
  birdsitelive: BirdsiteLIVE er en Twitter til ActivityPub-bro
  fedibird: Fedibird er en gratis, åpen kildekode sosial nettverksserver basert på ActivityPub, som er en gaffel av Mastodon
  notestock: ActivityPub Support SNS-innlegg (Mastodon, Misskey, Pleroma, etc) legges ut.
  mbin: en forent innholdsaggregator-, stemme-, diskusjons- og mikrobloggplattform (av fellesskapet, for fellesskapet)
  cherrypick: CherryPick er en åpen kildekode, desentralisert sosiale medieplattform som er gratis for alltid!
  meisskey: Drevet av Misskey
  sharkey: Sharkey er en åpen kildekode, desentralisert sosiale medieplattform som er gratis for alltid!
  hajkey: Hajkey er nok en gaffel av Misskey, som gir deg tullfrie rettelser, funksjoner og forbedringer du faktisk ønsker siden 2023
  iceshrimp: Iceshrimp er en desentralisert og forent sosial nettverkstjeneste som implementerer ActivityPub-standarden
  glitchcafe: Desentraliserte sosiale medier drevet av Mastodon
admin:
  add: Du kan legge til eller redigere en server du eier her
  domain: Serverdomene
  domainnote: Basisdomenenavnet til serveren din (uten etterfølgende skråstrek)
  email: Din e-post (valgfritt)
  emailnote: Vi deler aldri e-posten din med noen andre
  nodomain: ingen serverdomene gitt
  dupeserver: Serveren eksisterer allerede og er registrert til en eier, bruk redigeringsfunksjonen for å endre
  dnsrecord: Serveren eksisterer allerede, du kan gjøre krav på dette domenet ved å legge til en DNS TXT-post som sier
  dnsnote: Oppdater denne siden etter at du har oppdatert DNS-postene dine for å kunne legge til e-postadressen din på denne serveren
  emailsuccess: E-post lagt til domenet
  emailmissing: Gå tilbake og skriv inn e-postadressen du vil bruke på skjemaet
  addemailsubject: Ny server lagt til
  addemailbody: Ny server https://%(domain) lagt til databasen. Nye servere starter med en poengsum på 50, gi den litt tid til å bli sjekket og dukke opp.
  addsuccess: Data er satt inn! Serveren din vil bli sjekket og live på listen om noen timer!
  error: Kunne ikke validere serveren din, sjekk oppsettet ditt!<br>Ta en titt på <a href="//%(domain)/.well-known/nodeinfo">nodeinfo</a>
  edit: Vil du oppdatere serveren din?
  notoken: ingen token gitt
  noserver: ingen serverdomene gitt
  badtoken: dårlig token
  domainnotfound: domene ikke funnet
  mismatch: token mismatch
  expired: token utløpt
  deleted: server slettet
  systemdeleted: Serveren er blitt slettet, ingen oppdateringer kan gjøres, den kan ikke angres.
  paused: server satt på pause
  unpaused: serveren er satt på pause
  editemailsubject: Rediger varsel fra
  editemailbody: Data for %(domain) oppdatert. Vil bli sjekket på neste server-oppdatering, se statussiden.
  editsuccess: Data lagret
  authorized: Autorisert for å redigere <a href="https://%(server)/%(domain)">%(domain)</a> for %(hours)
  emailrequired: E-post
  termsurl: URL til vilkår
  ppurl: URL til personvernregler
  supporturl: 'URL til støtte (bruk mailto: hvis e-postadresse)'
  statement: Administratorerklæring (grense på 500 tegn)
  notify: Gi beskjed via e-post hvis serveren mislykkes i kontroller
  notifylevel: Gi beskjed når poengsummen din faller til
  status: Serverstatusen din er for øyeblikket
  delete: Slutt å sjekke og slutt å vise serveren din. Kan aktiveres på nytt senere.
  pause: Slutt å sjekke serveren din. Kan aktiveres på nytt senere.
  unpause: Unpause / Angre sletting - Begynn å sjekke og begynn å vise serveren din.
  tokenemailsubject: Rediger nøkkel for
  tokenemailbody: Denne lenken %(expires)
  tokensuccess: Link sendes til registrert e-post
  register: Denne serveren har ikke en e-post på filen, bruk <a href="/podmin">legg til en server-kobling</a> å registrere serveren din.
