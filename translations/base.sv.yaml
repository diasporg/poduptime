---
base:
  general:
    yes: 'Ja'
    no: 'Nej'
    up: Upp
    down: Ner
    close: Stänga
    next: Nästa
    back: Tillbaka
    server: Server
    servers: Servrar
    submit: Skicka in
    save: Spara
    delete: Radera
    pause: Paus
    unpause: Avbryt paus
    error: Fel
    date: Datum
    notice: Lägga märke till
    users: Användare
    comments: Kommentarer
    posts: Inlägg
    uptime: Drifttid
    latency: Latens
    manual: Manuell
    auto: Bil
    moreinfo: Detaljerad information om servern
    visitserver: Besök denna
    greenhost: Servrar i grönt identifieras som eco-hosted av Green Web Foundation
    api: Du kan hitta mer information om denna server med hjälp av vårt API, vi har begränsad information här
  navs:
    list: Lista
    map: Karta
    stats: Statistik
    monthlystats: En gång i månaden
    months: månader
    dailystats: Dagligen
    days: dagar
    software: Programvarufilter
    all: Allt
    search: Sök
    add: Lägg till
    edit: Redigera
    admin: Administration
    source: Källa
    api: API
    updated: Uppdaterad
    status: Status
    translation: Översätt
    terms: Villkor
    history: Historik
  strings:
    about: '%(sitename) hittar alla servrar i federationen och ger dig ett enkelt sätt att hitta ett hem'
    history:
      title: Historik över ändringar i servern
      domain: Domän
      change: Ändra
      note: Anteckning
      datechanged: Datum
      tip: Senaste ändringarna på servrar.
    list:
      navs:
        auto: Autoval
        autotip: Ett klick hittar du den bästa %(software) -servern
      columns:
        title: Listvy över
        server: Server
        serverdesc: En server är en webbplats där du kan skapa ditt konto
        name: namn
        namedesc: Definierat namn på denna server
        version: Version
        versiondesc: Version av programvara som denna server kör
        software: programvara
        softwaredesc: Typ av Fediverse-programvara som denna server kör
        uptime: Upptid
        uptimedesc: Procent av tiden som servern är online
        latency: Latens
        latencydesc: Genomsnittlig anslutningslatens i ms från %(hostlocation)
        signups: Registreringar
        signupsdesc: Tillåter denna server nya användare
        users: Användare
        activeusers: Aktiva användare
        usersdesc: Antal totalt användare på denna server
        users6: 6m
        users6desc: Antal användare aktiva de senaste 6 månaderna på denna server
        users1: 1m
        users1desc: Antal användare som varit aktiva senaste månaden på denna server
        posts: Inlägg
        postsdesc: Antal totalt inlägg på denna server
        comments: Kommentarer
        commentsdesc: Antal totalt kommentarer på denna server
        months: månader
        monthsdesc: Hur många månader har vi tittat på den här servern
        score: Göra
        scoredesc: Systempoäng på en 100 poängs skala
        status: Status
        statusdesc: System status
        country: Land
        countrydesc: Serverland, baserat på IP Geolocation
        city: Stad
        citydesc: Serverstad, baserad på IP Geolocation
        state: stat
        statedesc: Serverstatus, baserat på IP Geolocation
        language: Språk
        languagedesc: Serverspråk identifieras automatiskt från deras huvudsidas data
        greenhost: GreenHost
        greenhostdesc: Green Web Foundation identifierar detta som en grön värdserver
    map:
      title: Kartvy över
      tip: Använd programvarans menyväljare för att begränsa dessa data.
    search:
      software: programvara
      language: Språk
      location: Plats
      open: Öppen för registreringar
      results: '%(number) resultat för fråga: %(searchterm)'
      nextpage: Nästa sida
      lastpage: Sista sidan
      pagenumber: Sidonummer
      greenhost: GreenHost
    stats:
      title: Hela Fediverse
      users: Användare av programvara
      serverss: Servrar efter programvara
      serversc: Servrar per land
      serversh: Servrar av värd
      serversgreen: Gröna servrar
      statsfor: Månadsstatistik för
      average: Genomsnitt
      total: Totalt
      growth: Totalt antal användare per månad
      growthactive: Aktiva användare per månad
      serversper: Servrar online per månad
      halfyear: Aktiva användare halvår
      monthly: Aktiva användare varje månad
      commentsper: Kommentarer per månad
      postsper: Inlägg per månad
      tip: Pie diagram är topp 20. Alla grafer är i genomsnitt månadsdata för servrar som väljer att dela data till allmänheten. Använd programväljaren på menyn för att ändra dessa diagram till bara en programvara mot alla.
    dailystats:
      title: Hela Fediverse
      users: Användare efter programvara
      serverss: Servrar efter programvara
      serversc: Servrar per land
      serversh: Servrar av värd
      serversgreen: Gröna servrar
      statsfor: Daglig statistik för
      average: Genomsnitt
      total: Totalt
      growth: Totalt antal användare per dag
      growthactive: Aktiva användare per dag
      serversper: Servrar online per dag
      commentsper: Kommentarer per dag
      postsper: Inlägg efter dag
      tip: Pie diagram är topp 20. Alla grafer är genomsnittliga dagliga data för servrar som väljer att dela data till allmänheten. Använd programväljaren på menyn för att ändra dessa diagram till bara en programvara mot alla.
    singlepage:
      uptime: Drifttid & hastighet
      userstats: Användarstatistik
      actionstats: Åtgärdsstatistik
      clicksout: Klickar ut
      opensignup: Den här servern tillåter nya användare att registrera sig
      closedsignup: Den här servern accepterar inte nya användare
      lastchecked: Server senast kontrollerad
      notfound: Fediverse-servern hittades inte
      addit: Du kan alltid lägga till det
      deletedview: Visa raderad serverinformation
      private: Den här servern använder ett CDN för att blockera dess faktiska plats, du bör undersöka var den faktiska servern finns innan du använder den
      about: Handla om
      charts: Diagram
      data: Huvuddata
      metadata: Ytterligare metadata
      errors: Fel
      deleted: Servern har tagits bort
      findother: Vi kan hjälpa dig att hitta en ny %(software) -server
      goerror: Kunde inte välja automatiskt åt dig, inga servrar ser bra ut. Prova listan eller kartan för att välja en server manuellt.
      green: Denna webbplats använder en GreenHost
      name: Servernamn
      metatitle: Servertitel
      metadescription: Beskrivning
      detectedlanguage: Upptäckt språk
      ip: IP
      ipv6: IPv6
      greenhost: GreenHost
      host: Servervärd
      dnssec: DNSSEC
      servertype: HTTP-server
      terms: Villkor
      pp: Sekretesspolicy
      support: Stöd
      softwarename: Programvarans namn
      shortversion: Serverversion
      masterversion: Förvarsversion
      daysmonitored: Övervakade dagar
      date_laststats: Senast kontrollerad
      date_created: Datum tillagt
      countryname: Land
      city: Stad
      state: Ange
      zipcode: Postnummer
      uptime_alltime: Drifttidsprocent
      latency: Latens
      signup: Anmälningar öppna
      podmin_statement: Serverägares uttalande
    status:
      current: Systemstatus är
      red: Röd
      green: Grön
      status: och för närvarande
      idle: På tomgång
      running: Löpning
      last: Senast skannade uppdatering
      update: Senaste uppdateringen var
      updatetook: Senaste uppdateringen tog
      language: Senaste språkkontrollen var
      backup: Senaste säkerhetskopieringen av data var
      dbcleanup: Senaste datarensningen var
      dbvacuum: Senaste datavakuum var
      apipush: Senaste Push till API %(api) var
      rss: Senaste uppdateringen av RSS-filen var
      sitemap: Senaste uppdateringen av webbplatskartan var
      masterversion: Senaste Masterversion-uppdateringen från git repos var
      crawl: Senaste Fediverse crawl var
      monthly: Senaste månadsuppdatering av statistiktabellen var
      daily: Senaste uppdateringen av den dagliga statistiktabellen var
      poduptimeversion: Mjukvaru-version
      branch: Gren
      first: Den här sidan som ligger på %(location)hittade sin första server
      andtotal: och upptäckte totalt
    allsoftwares:
      title: Alla kända mjukvaror sorterade efter aktiva användare
welcome:
  main:
    find: Låt oss hitta ett hem för dig!
    suggested: Använd en föreslagen %(software) server nära dig
    picked: '%(domain) är en %(software) server på %(location)'
    nopicked: Inga bra servrar i närheten av %(location) för %(software) hittades, använd lista eller karta för att hitta fler
    first: Eller, Filtrera webbplatser efter programvara
    second: Filtrera sedan webbplatser efter vy
    map: Använd en <b class="fw-bold">-karta</b> för att hitta en server nära dig. <br>Detta kommer att göra saker snabbare för dig!
    list: Använd en <b class="fw-bold">-lista</b> med servrar för att hitta ett hem. <br>Du kan filtrera och sortera högst upp i listan.
softwares:
  diaspora: Diaspora är ett integritetsmedvetet, distribuerat socialt nätverk med öppen källkod
  friendica: Friendica är en decentraliserad kommunikationsplattform som integrerar social kommunikation
  hubzilla: Hubzilla är en kraftfull plattform för att skapa sammanlänkade webbplatser med en decentraliserad identitet
  pleroma: Pleroma är en gratis, federerad server för sociala nätverk byggd på öppna protokoll
  socialhome: Socialhome beskrivs bäst som en federerad personlig profil med sociala nätverksfunktioner
  social-relay: Ett reläsystem för förbundet inte för slutanvändning
  writefreely: WriteFreely är gratis programvara med öppen källkod för att bygga ett skrivutrymme på webben
  ganggo: GangGo är ett decentraliserat socialt nätverk skrivet i GoLang
  funkwhale: Funkwhale är ett gemenskapsdrivet projekt som låter dig lyssna och dela musik och ljud inom ett decentraliserat, öppet nätverk
  osada: Osada är ett makrobloggningsnätverk i konversationsstil som drivs av en social motor med hyperdriven, som stöder ActivityPub och Zot6-protokoll
  mastodon: Mastodon är ett decentraliserat socialt nätverk med öppen källkod – av folket för folket
  pixelfed: Pixelfed är en gratis och etisk fotodelningsplattform, driven av ActivityPub-federationen
  wordpress: Wordpress är programvara med öppen källkod som du kan använda för att enkelt skapa en vacker webbplats, blogg eller app
  misskey: Misskey är en decentraliserad mikrobloggplattform född på jorden
  speechmore: Speechmore är det sociala nätverket som ger dig kontroll över din data och ger dig friheten att säga vad du än måste
  peertube: PeerTube är en gratis och öppen källkod, decentraliserad, federerad videoplattform som drivs av ActivityPub och WebTorrent
  plume: Plume är en federerad bloggapplikation
  rustodon: Rustodon är en Mastodon-kompatibel federerad social mikrobloggserver
  microblogpub: Microblogpub är en egen värd, enanvändare, ActivityPub-driven mikroblogg
  mobilizon: Mobilizon är ett verktyg designat för att skapa plattformar för att hantera gemenskaper och evenemang
  lemmy: Lemmy är en länkaggregator med öppen källkod, som är lätt att vara värd för själv som du kan använda för att dela
  gnusocial: GNU social är den äldsta gratis sociala nätverksplattformen för offentlig och privat kommunikation som används i federerade sociala nätverk
  gotosocial: GoToSocial tillhandahåller en lätt, anpassningsbar och säkerhetsfokuserad ingång till Fediverse, och är jämförbar med (men skiljer sig från) befintliga projekt som Mastodon, Pleroma, Friendica och PixelFed.
  ecko: Ecko är en gemenskapsdriven gaffel av Mastodons sociala nätverksprogram. Tanken med gaffeln är att optimera mot gemenskap, det vill säga att göra det så enkelt som möjligt att bidra
  bookwyrm: BookWyrm är en plattform för social läsning! Du kan använda den för att spåra vad du läser, recensera böcker och följa dina vänner
  akkoma: en liten mikrobloggplattform, alias cooler pleroma
  calckey: Calckey är baserad på Misskey, en kraftfull mikrobloggserver på ActivityPub med funktioner som emoji-reaktioner, ett anpassningsbart webbgränssnitt, rik chatt och mycket mer!
  drupal: Med robusta verktyg för innehållshantering, sofistikerade API:er för flerkanalspublicering och en meritlista av kontinuerlig innovation – Drupal är den bästa digitala upplevelseplattformen (DXP) på webben
  epicyon: Epicyon är en federal server som är lämplig för att själv vara värd för ett litet antal konton på lågenergisystem
  foundkey: FoundKey är en gratis mikrobloggserver med öppen källkod som är kompatibel med ActivityPub. Forked from Misskey, FoundKey förbättrar underhållsbarhet och beteende, samtidigt som det tar in användbara funktioner
  gancio: en delad agenda för lokala samhällen (med aktivitetspubstöd)
  ktistec: Ktiste är en ActivityPub-server. Den är avsedd för enskilda användare
  owncast: Owncast är en öppen källkod, decentraliserad, decentraliserad livevideo- och chattserver för en användare för att köra dina egna liveströmmar som liknar de stora vanliga alternativen.
  firefish: Firefish är baserat på Misskey, en kraftfull mikrobloggserver på ActivityPub med funktioner som emoji-reaktioner, ett anpassningsbart webbgränssnitt, rik chatt och mycket mer!
  kbin: kbin är en reddit-liknande innehållsaggregator och mikrobloggplattform med öppen källkod för fediverse.
  birdsitelive: BirdsiteLIVE är en Twitter till ActivityPub-brygga
  fedibird: Fedibird är en gratis social nätverksserver med öppen källkod baserad på ActivityPub, som är en gaffel av Mastodon
  notestock: ActivityPub Support SNS-inlägg (Mastodon, Misskey, Pleroma, etc) läggs upp.
  mbin: en federerad innehållsaggregator, röstnings-, diskussions- och mikrobloggplattform (av gemenskapen, för gemenskapen)
  cherrypick: CherryPick är en decentraliserad plattform för sociala medier med öppen källkod som är gratis för alltid!
  meisskey: Drivs av Misskey
  sharkey: Sharkey är en öppen källkod, decentraliserad plattform för sociala medier som är gratis för alltid!
  hajkey: Hajkey är ännu en gaffel av Misskey, som ger dig no-nonsense fixar, funktioner och förbättringar som du faktiskt vill ha sedan 2023
  iceshrimp: Iceshrimp är en decentraliserad och federerad social nätverkstjänst som implementerar ActivityPub-standarden
  glitchcafe: Decentraliserade sociala medier som drivs av Mastodon
admin:
  add: Du kan lägga till eller redigera en server du äger här
  domain: Serverdomän
  domainnote: Basdomännamnet för din server (utan snedstreck)
  email: Din e-post (valfritt)
  emailnote: Vi kommer aldrig att dela din e-post med någon annan
  nodomain: ingen serverdomän ges
  dupeserver: Servern finns redan och är registrerad till en ägare, använd redigeringsfunktionen för att ändra
  dnsrecord: Servern finns redan, du kan göra anspråk på den här domänen genom att lägga till en DNS TXT-post som anger
  dnsnote: Uppdatera den här sidan efter att du har uppdaterat dina DNS-poster för att kunna lägga till din e-postadress på den här servern
  emailsuccess: E-post har lagts till i domänen
  emailmissing: Gå tillbaka och ange den e-postadress du vill använda i formuläret
  addemailsubject: Ny server har lagts till
  addemailbody: Ny server https://%(domain) har lagts till i databasen. Nya servrar börjar med en poäng på 50, ge det lite tid att kontrolleras och dyka upp.
  addsuccess: Data har infogats! Din server kommer att kontrolleras och visas på listan om några timmar!
  error: Det gick inte att validera din server, kontrollera dina inställningar!<br>Ta en titt på din <a href="//%(domain)/.well-known/nodeinfo">nodeinfo</a>
  edit: Vill du uppdatera din server?
  notoken: Ingen token ges
  noserver: ingen serverdomän ges
  badtoken: dålig token
  domainnotfound: domänen hittades inte
  mismatch: token mismatch
  expired: token har gått ut
  deleted: server raderad
  systemdeleted: Servern har tagits bort, inga uppdateringar kan göras, det kan inte vändas.
  paused: servern pausad
  unpaused: servern är pausad
  editemailsubject: Redigera meddelande från
  editemailbody: Data för %(domain) uppdaterad. Kontrolleras vid nästa serveruppdatering, se /statussida.
  editsuccess: Data sparas
  authorized: Auktoriserad att redigera <a href="https://%(server)/%(domain)">%(domain)</a> för %(hours)
  emailrequired: E-post
  termsurl: URL till termer
  ppurl: URL till sekretesspolicy
  supporturl: 'URL till support(använd mailto: om e-postadress)'
  statement: Administratörsuttalande (max 500 tecken)
  notify: Meddela via e-post om servern misslyckas med kontroller
  notifylevel: Meddela när din poäng faller till
  status: Din serverstatus är för närvarande
  delete: Sluta kontrollera och sluta visa din server. Kan återaktiveras senare.
  pause: Sluta kolla din server. Kan återaktiveras senare.
  unpause: Återställ/Återställ - Börja kontrollera och börja visa din server.
  tokenemailsubject: Redigera nyckel för
  tokenemailbody: Denna länk %(expires)
  tokensuccess: Länk skicka till registrerad e-post
  register: Den här servern har ingen e-post, använd <a href="/podmin">lägg till en server</a> att registrera din server.
